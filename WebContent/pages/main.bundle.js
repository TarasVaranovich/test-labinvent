webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/Comment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comment; });
var Comment = (function () {
    function Comment(parentID, messageText, userName) {
        this.parentID = parentID;
        this.messageText = messageText;
        this.userName = userName;
    }
    Comment.prototype.getParentId = function () {
        return this.parentID;
    };
    Comment.prototype.getMessageText = function () {
        return this.messageText;
    };
    Comment.prototype.getUserName = function () {
        return this.userName;
    };
    return Comment;
}());

//# sourceMappingURL=Comment.js.map

/***/ }),

/***/ "../../../../../src/app/CommentToDisplay.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentToDisplay; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Comment__ = __webpack_require__("../../../../../src/app/Comment.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var CommentToDisplay = (function (_super) {
    __extends(CommentToDisplay, _super);
    function CommentToDisplay(commentID, parentID, messageText, userName) {
        var _this = _super.call(this, parentID, messageText, userName) || this;
        _this.isChoosen = false;
        _this.isChoosen = false;
        _this.commentID = commentID;
        _this.uiOffset = 0;
        return _this;
    }
    CommentToDisplay.prototype.setUIOffset = function (uiOffsetValue) {
        this.uiOffset = uiOffsetValue;
    };
    CommentToDisplay.prototype.getUIOffset = function () {
        return this.uiOffset;
    };
    CommentToDisplay.prototype.isCommentChoosen = function () {
        return this.isChoosen;
    };
    CommentToDisplay.prototype.chooseComment = function () {
        this.isChoosen = true;
    };
    CommentToDisplay.prototype.unchooseComment = function () {
        this.isChoosen = false;
    };
    CommentToDisplay.buildTreeUI = function (comments) {
        var commentsUI = [];
        commentsUI = comments;
        for (var i = 0; i < commentsUI.length; i++) {
            if (i > 0) {
                if (commentsUI[i].getParentId() == commentsUI[i - 1].commentID) {
                    var newOffset = commentsUI[i].getUIOffset() + commentsUI[i - 1].getUIOffset() + 1;
                    commentsUI[i].setUIOffset(newOffset);
                }
                else {
                    var commentParents = commentsUI.filter(function (comment) { return comment.commentID === commentsUI[i].getParentId(); });
                    if (commentParents.length > 0) {
                        var newOffset = commentsUI[i].getUIOffset() + commentParents[0].getUIOffset() + 1;
                        commentsUI[i].setUIOffset(newOffset);
                    }
                    else {
                        if (commentsUI[i].getParentId() != 0) {
                            commentsUI[i].setUIOffset(null); // free comment
                        }
                    }
                }
            }
            else {
                if (commentsUI[i].getParentId() != 0) {
                    commentsUI[i].setUIOffset(null);
                }
            }
        }
        return commentsUI;
    };
    return CommentToDisplay;
}(__WEBPACK_IMPORTED_MODULE_0__Comment__["a" /* Comment */]));

//# sourceMappingURL=CommentToDisplay.js.map

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__article_article_component__ = __webpack_require__("../../../../../src/app/article/article.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search_component__ = __webpack_require__("../../../../../src/app/search/search.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: 'article', component: __WEBPACK_IMPORTED_MODULE_2__article_article_component__["a" /* ArticleComponent */] },
    { path: 'search', component: __WEBPACK_IMPORTED_MODULE_3__search_search_component__["a" /* SearchComponent */] }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgModule */])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]]
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__application_service__ = __webpack_require__("../../../../../src/app/application.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { Router } from '@angular/router';

var AppComponent = (function () {
    function AppComponent(applicationService) {
        var _this = this;
        this.applicationService = applicationService;
        this.title = 'Test for Labinvent';
        this.applicationAlertMessage = null;
        this.subscription = this.applicationService.getMessage().subscribe(function (applicationMessage) {
            _this.applicationAlertMessage = applicationMessage;
        });
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-root',
        template: "\n  <h1>{{title}}</h1>\n  <div>\n    <nav>\n      <a id=\"articleLink\" routerLink=\"/article\" routerLinkControl=\"active\">article</a>\n      <a id=\"userCommentsLink\" routerLink=\"/search\">search</a>\n    </nav>\n  </div>\n  <div>\n    <router-outlet></router-outlet>\n  </div>\n  <div style=\"display: block;\n              position: fixed;\n              width: 300px;\n              height: 50px;\n              top: 300px;\n              left: 50px;\n              background-color: yellow;\n              border: 2px solid black;\n              text-align: center;\" *ngIf=\"applicationAlertMessage\">\n              {{ applicationAlertMessage }}\n  </div>",
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__application_service__["a" /* ApplicationService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__article_article_component__ = __webpack_require__("../../../../../src/app/article/article.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__search_search_component__ = __webpack_require__("../../../../../src/app/search/search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__rest_service__ = __webpack_require__("../../../../../src/app/rest.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__application_service__ = __webpack_require__("../../../../../src/app/application.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








/*for rest-api work*/




/*components communication*/

var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__article_article_component__["a" /* ArticleComponent */],
            __WEBPACK_IMPORTED_MODULE_7__search_search_component__["a" /* SearchComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_5__app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["b" /* HttpClientModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_9__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_10__rest_service__["a" /* RestService */], __WEBPACK_IMPORTED_MODULE_11__application_service__["a" /* ApplicationService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/application.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ApplicationService = (function () {
    function ApplicationService() {
        this.subject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    ApplicationService.prototype.sendMessage = function (applicationMessage) {
        this.subject.next(applicationMessage);
    };
    ApplicationService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    return ApplicationService;
}());
ApplicationService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])()
], ApplicationService);

//# sourceMappingURL=application.service.js.map

/***/ }),

/***/ "../../../../../src/app/article/article.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/article/article.component.html":
/***/ (function(module, exports) {

module.exports = "<div>{{ articleTitle }}</div>\n<div>\n  <textarea cols=\"45\">\n    {{ articleContent }}\n  </textarea>\n</div>\n<div>\n  <p>Comments to article:</p>\n  <div style=\"background-color: grey; width: 400px; height: 30px; padding-top: 10px;\">\n    <button style=\"display: inline;\" (click)=\"getPreviousPage()\" [disabled]=\"previousBtnDisabled\">previous</button>\n    <p style=\"display: inline; color: white;\">{{ pageNumber }}</p>\n    <button (click)=\"getNextPage()\" style=\"display: inline;\" [disabled]=\"nextBtnDisabled\">next</button>\n    <p style=\"display: inline; color: white;\">Navigate to page:</p>\n    <input type=\"text\" size=\"5\" [(ngModel)]=\"srecificPageNumber\">\n    <button (click)=\"getSpecificPage(srecificPageNumber)\" style=\"display: inline;\">Go!</button>\n  </div>\n</div>\n<div style=\"position: relative; background-color: black; width: 400px; padding-left: 20px;\" *ngFor=\"let Comment of displayedComments\">\n  <div style=\"position: absolute;\n              width: 20px;\n              top:50px;\n              left: 0px;\n              padding-left: 3.5px;\n              padding-top: 1px;\n              border-radius: 10px;\n              box-sizing: border-box;\n              background-color: red;\" *ngIf=\"Comment.getUIOffset() == null\">\n    X\n  </div>\n  <!--<div style=\"width:100%; background-color: grey;\">-->\n  <div [ngStyle]=\"{'width': '100%','background-color':'grey', 'margin-left': Comment.getUIOffset()*20 + 'px'}\">\n    <p style=\"color: white;\">{{Comment.userName}} wrote:</p>\n    <textarea rows=\"10\" cols=\"45\" disabled=\"disabled\">{{Comment.messageText}}</textarea>\n    <div>\n      <span style=\"color: yellow;\">id: {{Comment.commentID}}</span>\n      <span style=\"color: yellow;\">parent id: {{Comment.parentID}}</span>\n    </div> <!--style=\"display: none;\"-->\n    <button (click)=\"annotateComment(Comment.commentID)\">Annotate</button>\n    <div style=\"background-color: black; width: 400px;\" *ngIf=\"choosenCommentId == Comment.commentID\">\n      <textarea rows=\"10\" cols=\"45\" [(ngModel)]=\"Comment.annotationText\"></textarea>\n      <div>\n        <p style=\"display: inline; color: white;\">Your name:<input type=\"text\" size=\"10\" [(ngModel)]=\"Comment.annotatingUser\"></p>\n        <button (click)=\"sendComment(Comment.commentID)\">Send</button><button (click)=\"cancelAnnotation()\">Cancel</button>\n      </div>\n    </div>\n  </div>\n</div>\n<div style=\"background-color: blue; width: 400px; padding-left: 20px;\">\n  <textarea rows=\"10\" cols=\"45\" [(ngModel)]=\"globalCommentMessage\">Comment</textarea>\n  <div>\n    <p style=\"display: inline; color: white;\">Your name:<input type=\"text\" size=\"10\" [(ngModel)]=\"globalCommentUser\"></p>\n    <button (click)=\"sendComment(0)\">Send</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/article/article.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArticleComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Comment__ = __webpack_require__("../../../../../src/app/Comment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__CommentToDisplay__ = __webpack_require__("../../../../../src/app/CommentToDisplay.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__rest_service__ = __webpack_require__("../../../../../src/app/rest.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__application_service__ = __webpack_require__("../../../../../src/app/application.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ArticleComponent = (function () {
    function ArticleComponent(communicationService, applicationService) {
        this.communicationService = communicationService;
        this.applicationService = applicationService;
        this.displayedComments = [];
        this.applicationAlertMessage = 'Please wait for server answer...';
    }
    ArticleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.articleTitle = "Lorem..."; //alert("Rsult" + result)
        this.articleContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        this.communicationService.getUserKey().then(function (result) { return _this.getUserKey(result.toString()); }).catch(function (error) { return alert("Error occured:" + error); });
        this.choosenCommentId = null;
        /*init setting*/
        this.pageType = "desktop";
        this.pageSize = 4; /*Page size in comments*/
        this.pageNumber = 1;
        this.previousBtnDisabled = false;
        this.nextBtnDisabled = false;
        this.applicationService.sendMessage(null);
    };
    ArticleComponent.prototype.getUserKey = function (resultString) {
        this.userKey = resultString;
        //this.communicationService.getPage(this.userKey, this.pageNumber, this.pageSize, this.pageType).then((result: CommentToDisplay[]) => this.getPageComments(result)).catch(error => alert("Error occured:" + error));
        this.getSpecificPage(this.pageNumber, 0);
    };
    ArticleComponent.prototype.annotateComment = function (commentId) {
        this.choosenCommentId = commentId;
    };
    ArticleComponent.prototype.cancelAnnotation = function () {
        this.choosenCommentId = null;
    };
    ArticleComponent.prototype.getPageComments = function (requestedPageNumber, responceComments, pageRoute) {
        if (responceComments.length > 0) {
            this.displayedComments = responceComments;
            this.pageNumber = requestedPageNumber;
            this.displayedComments = __WEBPACK_IMPORTED_MODULE_2__CommentToDisplay__["a" /* CommentToDisplay */].buildTreeUI(this.displayedComments);
            //check dispalyed comments change
            /*for(let comm of this.displayedComments){
              console.log(comm.commentID + ":" +
                          comm.getParentId() + ":" +
                          comm.getUIOffset());
            }*/
            //end check dispalyed comments change
            this.applicationService.sendMessage(null);
        }
        else {
            switch (pageRoute) {
                case 1:
                    this.nextBtnDisabled = true;
                    break;
                case -1:
                    this.previousBtnDisabled = true;
                    break;
                default:
                    break;
            }
            this.applicationService.sendMessage(null);
        }
    };
    ArticleComponent.prototype.sendComment = function (commentId) {
        var _this = this;
        var newComment;
        if (commentId > 0) {
            var currentComment = this.displayedComments.find(function (x) { return x.commentID == commentId; });
            newComment = new __WEBPACK_IMPORTED_MODULE_1__Comment__["a" /* Comment */](currentComment.commentID, currentComment.annotationText, currentComment.annotatingUser);
            this.displayedComments.find(function (x) { return x.commentID == commentId; }).annotatingUser = "";
            this.displayedComments.find(function (x) { return x.commentID == commentId; }).annotationText = "";
        }
        else {
            newComment = new __WEBPACK_IMPORTED_MODULE_1__Comment__["a" /* Comment */](0, this.globalCommentMessage, this.globalCommentUser);
            this.globalCommentMessage = "";
            this.globalCommentUser = "";
        }
        this.communicationService.addComment(newComment, this.userKey, this.pageType).then(function (result) { return _this.sendingCommentResult(result.toString()); }).catch(function (error) { return alert("Error occured:" + error); });
        this.applicationService.sendMessage(this.applicationAlertMessage);
        this.choosenCommentId = null;
    };
    ArticleComponent.prototype.sendingCommentResult = function (resultString) {
        var _this = this;
        var resultMessage;
        if (resultString == "false") {
            //AAA!!
            //alert("Comment not added :(");
            resultMessage = "Comment not added :(";
        }
        else if (resultString == "true") {
            //AAA!!
            //alert("Comment added successfully.");
            resultMessage = "Comment added successfully.";
            this.getSpecificPage(this.pageNumber, 0);
        }
        //this.applicationService.sendMessage(null);
        setTimeout(function () {
            _this.applicationService.sendMessage(resultMessage);
            console.log("After timeout...");
        }, 500);
        setTimeout(function () {
            _this.applicationService.sendMessage(null);
            console.log("After timeout...");
        }, 3000);
    };
    ArticleComponent.prototype.getPreviousPage = function () {
        var previousPageNumber;
        previousPageNumber = this.pageNumber - 1;
        this.getSpecificPage(previousPageNumber, -1);
        this.nextBtnDisabled = false;
    };
    ArticleComponent.prototype.getNextPage = function () {
        var nextPageNumber;
        nextPageNumber = this.pageNumber + 1;
        this.getSpecificPage(nextPageNumber, 1);
        this.previousBtnDisabled = false;
    };
    ArticleComponent.prototype.getSpecificPage = function (specificPageNumber, pageRoute) {
        var _this = this;
        var resultSize;
        this.applicationService.sendMessage(this.applicationAlertMessage);
        this.communicationService.getPage(this.userKey, specificPageNumber, this.pageSize, this.pageType).then(function (result) { return _this.getPageComments(specificPageNumber, result, pageRoute); }).catch(function (error) { return alert("Error occured:" + error); });
        console.log("Get page number" + specificPageNumber);
    };
    return ArticleComponent;
}());
ArticleComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'article',
        template: __webpack_require__("../../../../../src/app/article/article.component.html"),
        styles: [__webpack_require__("../../../../../src/app/article/article.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__rest_service__["a" /* RestService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__rest_service__["a" /* RestService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__application_service__["a" /* ApplicationService */]) === "function" && _b || Object])
], ArticleComponent);

var _a, _b;
//# sourceMappingURL=article.component.js.map

/***/ }),

/***/ "../../../../../src/app/rest.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__CommentToDisplay__ = __webpack_require__("../../../../../src/app/CommentToDisplay.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RestService = (function () {
    function RestService(http) {
        this.http = http;
    }
    //ARTICLE FORM
    RestService.prototype.addComment = function (comment, userUUID, pageType) {
        var commentJSON = JSON.stringify({ userName: comment.getUserName(),
            messageText: comment.getMessageText(),
            parentID: comment.getParentId() });
        return this.http.post('http://localhost:8080/test-labinvent/getcomments.htm/addcomment.htm?userUUID=' +
            userUUID
            + '&pageType='
            + pageType, commentJSON.toString(), { responseType: 'text' }).toPromise().then(this.extractUserKey).catch(this.handleError);
        ;
    };
    RestService.prototype.getUserKey = function () {
        return this.http.post('http://localhost:8080//test-labinvent/index.htm', null, { responseType: 'text' }).toPromise().then(this.extractUserKey).catch(this.handleError);
    };
    RestService.prototype.getPage = function (userKey, pageNumber, pageSize, pageType) {
        return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getspecificpage.htm?userUUID=' +
            userKey +
            '&pagenumber=' +
            pageNumber +
            '&pageSize=' + pageSize +
            '&pageType=' + pageType, { responseType: 'text' }).toPromise().then(this.extractPage).catch(this.handleError);
    };
    //END ARTICLE FORM
    //SEARCH FORM
    RestService.prototype.getUserComments = function (userName, pageSizeSearch, pageNumberSearch, commentUUID) {
        return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getusercomments.htm?userName=' +
            userName +
            '&pageSizeSearch=' +
            pageSizeSearch +
            '&pageNumberSearch=' +
            pageNumberSearch +
            '&commentUUID=' + commentUUID, { responseType: 'text' }).toPromise().then(this.extractPage).catch(this.handleError);
    };
    RestService.prototype.getCommentsContainsText = function (predeterminedText, pageSizeSearch, pageNumberSearch, commentUUID) {
        return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getcommentscontainstext.htm?predeterminedtext=' +
            predeterminedText +
            '&pageSizeSearch=' +
            pageSizeSearch +
            '&pageNumberSearch=' +
            pageNumberSearch +
            '&commentUUID=' + commentUUID, { responseType: 'text' }).toPromise().then(this.extractPage).catch(this.handleError);
    };
    RestService.prototype.getDialogPage = function (pageSizeDialog, pageNumberDialog, commentUUIDDialog) {
        return this.http.get('http://localhost:8080//test-labinvent/getcomments.htm/getdialogpage.htm?pageSizeDialog=' +
            pageSizeDialog +
            '&pageNumberDialog=' +
            pageNumberDialog +
            '&commentUUIDDialog=' +
            commentUUIDDialog, { responseType: 'text' }).toPromise().then(this.extractPage).catch(this.handleError);
    };
    //END SEARCH FORM
    RestService.prototype.extractUserKey = function (userKeyString) {
        return userKeyString;
    };
    RestService.prototype.extractPage = function (pageResopnse) {
        var commentsArray = JSON.parse(pageResopnse);
        var responceComments = [];
        for (var index in commentsArray) {
            var comment = new __WEBPACK_IMPORTED_MODULE_2__CommentToDisplay__["a" /* CommentToDisplay */](commentsArray[index].id, commentsArray[index].parentid, commentsArray[index].messageText, commentsArray[index].userName);
            responceComments.push(comment);
        }
        return responceComments;
    };
    RestService.prototype.handleError = function (error) {
        //alert('An error occurred' + error.error.message);
        return Promise.reject(error.message || error);
    };
    return RestService;
}());
RestService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], RestService);

var _a;
//# sourceMappingURL=rest.service.js.map

/***/ }),

/***/ "../../../../../src/app/search/search.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<div>{{ searchTitle }}</div>\n<div style=\"background-color: grey; width: 400px;\">\n  <p style=\"color: white;\">Input a search predicate:</p>\n  <textarea rows=\"10\" cols=\"45\" [(ngModel)]=\"searchPredicate\"></textarea>\n  <div>\n    <p style=\"display: inline;\">By user name</p>\n    <input type=\"radio\" [(ngModel)]=\"searchType\" value=\"username\">\n    <p style=\"display: inline;\">By text in comment</p>\n    <input type=\"radio\" [(ngModel)]=\"searchType\" value=\"commenttext\">\n  </div>\n  <button (click)=\"searchComments(searchPredicate, pageNumberSearch)\" style=\"display: inline;\">Search!</button>\n</div>\n<p>Result comments:</p>\n<div style=\"background-color: grey; width: 400px; height: 30px; padding-top: 10px;\">\n  <button style=\"display: inline;\" (click)=\"getPreviousPageSearch()\" [disabled]=\"previousBtnDisabled\">previous</button>\n  <p style=\"display: inline; color: white;\">{{ pageNumberSearch }}</p>\n  <button (click)=\"getNextPageSearch()\" style=\"display: inline;\" [disabled]=\"nextBtnDisabled\">next</button>\n</div>\n<div style=\"background-color: grey; width: 400px; border-bottom: 4px solid white;\" *ngFor=\"let Comment of displayedComments\">\n  <p style=\"display: inline; color: white;\">{{Comment.userName}} wrote:</p>\n  <button style=\"display: inline; margin-left: 40px;\" (click)=\"openDialogBranch(Comment.commentID)\">Open dialog branch</button>\n  <textarea rows=\"10\" cols=\"45\" disabled=\"disabled\">{{Comment.messageText}}</textarea>\n  <div>\n    <span style=\"color: yellow;\">id: {{Comment.commentID}}</span>\n    <span style=\"color: yellow;\">parent id: {{Comment.parentID}}</span>\n  </div>\n</div>\n<div style=\"display: block;\n            position: fixed;\n            width: auto;\n            max-height: 600px;\n            top: 10px;\n            left: 20px;\n            background-color: grey;\n            border: 2px solid black;\n            overflow: scroll;\"\n            *ngIf=\"dialogID\">\n            <div>\n              <p style=\"display: inline; color: white;\">\n                Comment\n                <span style=\"color: yellow;\">{{ dialogID }}</span>\n                conversation\n              </p>\n              <button style=\"display: inline; float:right;\" (click)=\"closeDialogBranch()\">Close</button>\n            </div>\n            <div>\n                <button style=\"display: inline;\" (click)=\"getPreviousPageDialog()\" [disabled]=\"previousBtnDialogDisabled\">Previous</button>\n                <p style=\"display: inline; color: white;\">{{ pageNumberDialog }}</p>\n                <button style=\"display: inline;\" (click)=\"getNextPageDialog()\" [disabled]=\"nextBtnDialogDisabled\">Next</button>\n            </div>\n            <div style=\"position: relative; background-color: black; width: 400px; padding-left: 20px; border-bottom: 4px solid white;\" *ngFor=\"let Comment of dialogComments\">\n              <div style=\"position: absolute;\n                          width: 20px;\n                          top:50px;\n                          left: 0px;\n                          padding-left: 3.5px;\n                          padding-top: 1px;\n                          border-radius: 10px;\n                          box-sizing: border-box;\n                          background-color: red;\" *ngIf=\"Comment.getUIOffset() == null\">\n                X\n              </div>\n              <div [ngStyle]=\"{'width': '100%','background-color':'grey', 'margin-left': Comment.getUIOffset()*20 + 'px'}\">\n                <p style=\"display: inline; color: white;\">{{Comment.userName}} wrote:</p>\n                <textarea rows=\"10\" cols=\"45\" disabled=\"disabled\">{{Comment.messageText}}</textarea>\n                <div>\n                  <span style=\"color: yellow;\">id: {{Comment.commentID}}</span>\n                  <span style=\"color: yellow;\">parent id: {{Comment.parentID}}</span>\n                </div>\n              </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__CommentToDisplay__ = __webpack_require__("../../../../../src/app/CommentToDisplay.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rest_service__ = __webpack_require__("../../../../../src/app/rest.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__application_service__ = __webpack_require__("../../../../../src/app/application.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchComponent = (function () {
    function SearchComponent(communicationService, applicationService) {
        this.communicationService = communicationService;
        this.applicationService = applicationService;
        this.displayedComments = [];
        this.searchPredicate = "";
        this.dialogComments = [];
        this.applicationAlertMessage = 'Please wait for server answer...';
    }
    SearchComponent.prototype.ngOnInit = function () {
        this.searchTitle = "Search comments";
        this.searchType = "username";
        var commentOne = new __WEBPACK_IMPORTED_MODULE_1__CommentToDisplay__["a" /* CommentToDisplay */](0, 0, "Message One", "User One");
        this.displayedComments.push(commentOne);
        this.commentKey = "searchedComment";
        this.pageNumberSearch = 1;
        this.pageSizeSearch = 4;
        this.dialogID = null;
        //this.dialogID = 99;
        this.pageNumberDialog = 1;
        this.pageSizeDialog = 4;
        this.dialogComments.push(commentOne);
        this.applicationService.sendMessage(null);
        this.previousBtnDisabled = false;
        this.nextBtnDisabled = false;
        this.previousBtnDialogDisabled = false;
        this.nextBtnDialogDisabled = false;
    };
    SearchComponent.prototype.searchComments = function (searchPredicate, pageNumberSearch) {
        var _this = this;
        if (this.searchPredicate != "") {
            //alert(this.searchPredicate + ":" + this.searchType);
            switch (this.searchType) {
                case "username": {
                    this.communicationService.getUserComments(this.searchPredicate, this.pageSizeSearch, pageNumberSearch - 1, this.commentKey).then(function (result) { return _this.getPageCommentsSearch(result, pageNumberSearch); }).catch(function (error) { return alert("Error occured:" + error); });
                    this.applicationService.sendMessage(this.applicationAlertMessage);
                    break;
                }
                case "commenttext": {
                    this.communicationService.getCommentsContainsText(this.searchPredicate, this.pageSizeSearch, pageNumberSearch - 1, this.commentKey).then(function (result) { return _this.getPageCommentsSearch(result, pageNumberSearch); }).catch(function (error) { return alert("Error occured:" + error); });
                    this.applicationService.sendMessage(this.applicationAlertMessage);
                    break;
                }
                default: {
                    alert("Error!");
                    break;
                }
            }
        }
        else {
            this.applicationService.sendMessage("Please input a required text.");
            setTimeout(function () {
                _this.applicationService.sendMessage(null);
                console.log("After timeout...");
            }, 2000);
        }
    };
    SearchComponent.prototype.getPageCommentsSearch = function (responceComments, pageNumberSearch) {
        var _this = this;
        if (responceComments.length > 0) {
            this.displayedComments = responceComments;
            this.pageNumberSearch = pageNumberSearch;
        }
        else {
            if (this.pageNumberSearch < pageNumberSearch) {
                this.nextBtnDisabled = true;
            }
            else if (this.pageNumberSearch > pageNumberSearch) {
                this.previousBtnDisabled = true;
            }
            else {
                this.applicationService.sendMessage("Search doesn't produce result.");
            }
        }
        setTimeout(function () {
            _this.applicationService.sendMessage(null);
            console.log("After timeout...");
        }, 2000);
    };
    SearchComponent.prototype.getPreviousPageSearch = function () {
        var previousPageNumber = this.pageNumberSearch - 1;
        this.searchComments(this.searchPredicate, previousPageNumber);
        this.nextBtnDisabled = false;
    };
    SearchComponent.prototype.getNextPageSearch = function () {
        var nextPageNumber = this.pageNumberSearch + 1;
        this.searchComments(this.searchPredicate, nextPageNumber);
        this.previousBtnDisabled = false;
    };
    //DIALOG METHODS
    SearchComponent.prototype.openDialogBranch = function (commentID) {
        this.dialogID = commentID;
        this.pageNumberDialog = 1;
        this.getDialogPage(this.pageNumberDialog, this.dialogID);
        this.nextBtnDialogDisabled = false;
        this.previousBtnDialogDisabled = false;
        //ELSE GET -1 PAGE NUMBER
        //this.pageNumberDialog = -1
        //this.getDialogPage(this.pageNumberDialog, this.dialogID);
    };
    SearchComponent.prototype.getDialogPage = function (pageNumberDialog, dialogID) {
        var _this = this;
        this.communicationService.getDialogPage(this.pageSizeDialog, pageNumberDialog, this.commentKey + dialogID.toString()).then(function (result) { return _this.getPageCommentsDialog(result, pageNumberDialog); }).catch(function (error) { return alert("Error occured:" + error); });
        this.applicationService.sendMessage(this.applicationAlertMessage);
    };
    SearchComponent.prototype.closeDialogBranch = function () {
        this.dialogID = null;
        this.dialogComments = [];
    };
    SearchComponent.prototype.getPreviousPageDialog = function () {
        var previousPageNumber = this.pageNumberDialog - 1;
        if (previousPageNumber == 0) {
            previousPageNumber = -1;
        }
        console.log("Getting dialog page:" + previousPageNumber);
        this.getDialogPage(previousPageNumber, this.dialogID);
        this.nextBtnDialogDisabled = false;
    };
    SearchComponent.prototype.getNextPageDialog = function () {
        var nextPageNumber = this.pageNumberDialog + 1;
        if (nextPageNumber == 0) {
            nextPageNumber = 1;
        }
        console.log("Getting dialog page:" + nextPageNumber);
        this.getDialogPage(nextPageNumber, this.dialogID);
        this.previousBtnDialogDisabled = false;
    };
    SearchComponent.prototype.getPageCommentsDialog = function (responseComments, pageNumberDialog) {
        if (responseComments.length > 0) {
            this.dialogComments = responseComments;
            this.pageNumberDialog = pageNumberDialog;
            this.dialogComments = __WEBPACK_IMPORTED_MODULE_1__CommentToDisplay__["a" /* CommentToDisplay */].buildTreeUI(this.dialogComments);
            //test
            /*for(let comm of this.dialogComments) {
              console.log("id-" + comm.commentID + "-pid-" + comm.getParentId() + "-off-" + comm.getUIOffset());
            }*/
            //end test
        }
        else {
            if (pageNumberDialog > 0) {
                this.nextBtnDialogDisabled = true;
            }
            else if (pageNumberDialog < 0) {
                this.previousBtnDialogDisabled = true;
            }
            else {
                alert("Search doesn't produce result.");
            }
            //this.dialogComments = [];
            //this.closeDialogBranch();
        }
        this.applicationService.sendMessage(null);
    };
    return SearchComponent;
}());
SearchComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'search',
        template: __webpack_require__("../../../../../src/app/search/search.component.html"),
        styles: [__webpack_require__("../../../../../src/app/search/search.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__rest_service__["a" /* RestService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__rest_service__["a" /* RestService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__application_service__["a" /* ApplicationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__application_service__["a" /* ApplicationService */]) === "function" && _b || Object])
], SearchComponent);

var _a, _b;
//# sourceMappingURL=search.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map