package testlabinvent.data_redis.model;

import java.util.List;
import java.io.Serializable;

public class CommentsCache implements Serializable{
	
	private String UUIDString;
	private List<Integer> childsIDs;
	private Integer commentParent;
	
	public CommentsCache(String sessionUUID, List<Integer>childsIDs, Integer commentParentId) {
		
		this.UUIDString = sessionUUID;
		this.childsIDs = childsIDs;
		this.commentParent = commentParentId;
	}
	
	public String getUUIDString() {
		
		return this.UUIDString;
	}
	
	public List<Integer> getChildsIDs() {
		
		return this.childsIDs;
	}
	
	public Integer getCommentParent() {
		
		return this.commentParent;
	}
	
}
