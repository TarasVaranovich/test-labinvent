package testlabinvent.data_redis.services;
import testlabinvent.data_redis.model.CommentsCache;

public interface CommentsCacheService {
	
	public void saveCommentsCache(CommentsCache commentsCache);
	public CommentsCache getCommentsCacheById(String commentsCacheUUID);
	public void deleteCommentsCacheById(String commentsCacheUUID);
}
