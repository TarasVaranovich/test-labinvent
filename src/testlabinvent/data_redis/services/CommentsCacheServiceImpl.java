package testlabinvent.data_redis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import testlabinvent.data_redis.jpa.CommentsCacheRepository;
import testlabinvent.data_redis.model.CommentsCache;

@Service
@Scope
public class CommentsCacheServiceImpl implements CommentsCacheService{
	
	private final CommentsCacheRepository commentsCacheRepository;
	
	@Autowired(required = true)
	public CommentsCacheServiceImpl(CommentsCacheRepository commentsCacheRepository) {
		
		this.commentsCacheRepository = commentsCacheRepository;
	}
	
	public void saveCommentsCache(CommentsCache commentsCache){
		
		commentsCacheRepository.saveCommentsCache(commentsCache);
		
	}
	
	public CommentsCache getCommentsCacheById(String commentsCacheUUID) {
		
		return commentsCacheRepository.findCommentsCache(commentsCacheUUID);
	}
	
	public void deleteCommentsCacheById(String commentsCacheUUID) {
		
		commentsCacheRepository.deleteCommentsCache(commentsCacheUUID);
		
	}
}
