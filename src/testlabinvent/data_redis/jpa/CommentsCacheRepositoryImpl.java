package testlabinvent.data_redis.jpa;

import org.springframework.stereotype.Repository;

import testlabinvent.data_redis.model.CommentsCache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.HashOperations;
import javax.annotation.PostConstruct;

@Repository
public class CommentsCacheRepositoryImpl implements CommentsCacheRepository{
	
	private static final String KEY = "test_labinvent_comments_cahe_database";
	private HashOperations hashOps;
	private RedisTemplate<String, Object> redisTemplate;
	
	@Autowired(required = true)
	private CommentsCacheRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
		
		this.redisTemplate = redisTemplate;
	}
	
	@PostConstruct
    private void init() {
		
        this.hashOps = redisTemplate.opsForHash();
    }
	
	public void saveCommentsCache(CommentsCache commentsCache){
		
		hashOps.put(KEY, commentsCache.getUUIDString(), commentsCache);
	}
	
	public CommentsCache findCommentsCache(String commentsCacheId) {
		
		try{
			
			Object cc = hashOps.get(KEY, commentsCacheId);
			
		} catch(Exception e) {
			
			System.out.println("Exception performed...");
			e.printStackTrace();
		}
		
		return (CommentsCache) hashOps.get(KEY, commentsCacheId);
	}
	
	public void deleteCommentsCache(String commentsCacheId) {
		
		hashOps.delete(KEY, commentsCacheId);
	}
	
}
