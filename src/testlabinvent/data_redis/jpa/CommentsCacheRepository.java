package testlabinvent.data_redis.jpa;

import testlabinvent.data_redis.model.CommentsCache;

public interface CommentsCacheRepository {
	
	void saveCommentsCache(CommentsCache commentsCache);
	CommentsCache findCommentsCache(String commentsCacheId);
	void deleteCommentsCache(String commentsCacheId);
}
