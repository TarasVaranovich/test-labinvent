package testlabinvent.listeners;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.context.event.ContextClosedEvent;

import testlabinvent.usecases.FillDataComponent;
import testlabinvent.usecases.GetCommentsComponent;
import testlabinvent.usecases.SearchCommentsComponent;
import testlabinvent.web.CommentsController;

@Component
@Scope("singleton")
public class ApplicationContextLoadListener implements ApplicationListener{

	private FillDataComponent fillDataComponent;
	private GetCommentsComponent getCommentsComponent;
	private SearchCommentsComponent searchCommentsComponent;
		
	@Override
    public void onApplicationEvent(ApplicationEvent event) {
		
        if (event instanceof ContextRefreshedEvent) {
            ApplicationContext applicationContext = ((ContextRefreshedEvent) event).getApplicationContext();
            this.fillDataComponent = (FillDataComponent)applicationContext.getBean("fillDatabaseBean");
            this.getCommentsComponent = (GetCommentsComponent)applicationContext.getBean("getCommentsBean");
            this.searchCommentsComponent = (SearchCommentsComponent)applicationContext.getBean("searchCommentsBean");
            System.out.println("Application Context Initialized...");
            if(fillDataComponent == null) {
            	
            	System.out.println("FillData component not initialized...");
            	
            }
            
            try {
            	
            	fillDataComponent.fillData();
            	 
            } catch (Exception e) {
            	
            	System.out.println("Problems with filled data");
            	
            	e.printStackTrace();
            }
            
            if(getCommentsComponent == null) {
            	
            	System.out.println("GetCommentsComponent component not initialized in application aontext listener...");
            	
            }
          
        } else if(event instanceof ContextClosedEvent) {
        	
        	try {
        		
        		for(String pageType : CommentsController.pageTypesSet) {
        			
        			 getCommentsComponent.updatePagesCache("clear", "desktop");
        		}
                	 
            } catch(Exception e) {
                	 
               System.out.println("Problems with cleaning pages cache after application stop.");
                	e.printStackTrace();
            }
        	
        	try {
        		
        		for(String commentUUID: CommentsController.commentUUIDsSet) {
        			
        			 searchCommentsComponent.clearCache(commentUUID);
        		}
                	 
            } catch(Exception e) {
                	 
               System.out.println("Problems with cleaning searched comments cache after application stop.");
                	e.printStackTrace();
            }
        	System.out.println("Application Context Closed...");   
        	
        } else {
        	
        	System.out.println("Event:" + event.toString());
        }
    }
	
}
