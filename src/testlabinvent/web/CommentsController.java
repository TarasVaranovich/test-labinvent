package testlabinvent.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.fasterxml.jackson.databind.node.ArrayNode;

import testlabinvent.usecases.SearchCommentsComponent;
import testlabinvent.usecases.GetCommentsComponent;
import testlabinvent.usecases.AddCommentsComponent;
import testlabinvent.data.model.Comment;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin
@Controller
@RequestMapping("/getcomments.htm")
public class CommentsController {
	
	public static Set<String> pageTypesSet = new HashSet<String>();
	public static Set<String> commentUUIDsSet = new HashSet<String>();
	
	@Autowired(required = true)
	@Qualifier("searchCommentsBean")
	SearchCommentsComponent searchCommentsComponent;
	
	@Autowired(required = true)
	@Qualifier("getCommentsBean")
	GetCommentsComponent getCommentsComponent;
	
	@Autowired(required = true)
	@Qualifier("addCommentsBean")
	AddCommentsComponent addCommentsComponent;
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value="getfirstpage.htm",method = GET) //, consumes = MediaType.APPLICATION_JSON_VALUE
	public String getFirstPage(Model model, 
								@RequestParam(value = "userUUID", required = true) String userUUID,
								@RequestParam(value = "pageSize", required = true) Integer pageSize) {
		System.out.println("Getting first page...");
		System.out.println("UserId" + userUUID);
		
		return "firstpage-YO";
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED) //,  consumes = MediaType.APPLICATION_JSON_VALUE
	@ResponseBody
	@RequestMapping(value="getspecificpage.htm",method = GET) //, consumes = MediaType.APPLICATION_JSON_VALUE
	public String getSpecificPage(Model model, 
			@RequestParam(value = "userUUID", required = true) String userUUID, 
			@RequestParam(value = "pagenumber", required = true) Integer pageNumber, 
			@RequestParam(value = "pageSize", required = true) Integer pageSize,
			@RequestParam(value = "pageType", required = true) String pageType) {
		try{
			pageTypesSet.add(pageType);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("UserId->" + userUUID + "<-pageNumber->" + pageNumber + "<-PageSize->" + pageSize);
		System.out.println("Begin page recieving:" + System.currentTimeMillis());
		List<Comment> pageComments = new ArrayList<Comment>();
		pageComments = getCommentsComponent.getSpecificPage(userUUID, pageSize, pageNumber,  pageType);
		
		//map to json
		ObjectMapper commentsMapper = new ObjectMapper();
		ArrayNode commentsArray = commentsMapper.createArrayNode();
		for(Comment comment: pageComments) {
			ObjectNode commentNode = commentsMapper.createObjectNode();
			commentNode.put("id", comment.getId());
			commentNode.put("parentid", comment.getParentId());
			commentNode.put("userName", comment.getUserName());
			commentNode.put("messageText", comment.getMessageText());
			
			commentsArray.add(commentNode);
		}
		System.out.println("Page returned:" + System.currentTimeMillis());
		return commentsArray.toString();
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value="addcomment.htm",method = POST) 
	public String addComment(Model model, 
			@RequestBody String commentJSON, 
			@RequestParam(value = "userUUID", required = true) String userUUID, 
			@RequestParam(value = "pageType", required = true) String pageType) {
		System.out.println("Request JSON:" + commentJSON);
		ObjectMapper newCommentMapper = new ObjectMapper();	
		//get from json
		try {
			
			ObjectNode commentNode = newCommentMapper.readValue(commentJSON, ObjectNode.class);		
			Comment comment = new Comment(commentNode.get("messageText").asText(),
											commentNode.get("userName").asText(),
											commentNode.get("parentID").asInt());
			
			if(addCommentsComponent.addComment(comment)) {
;											
				getCommentsComponent.updatePagesCache(userUUID, pageType);
				
				return "true";
				
			} else {
				
				return "false";
			}
			
		} catch (Exception  e) {
			
			e.printStackTrace();
			
			return "false";
		}		
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value="getusercomments.htm",method = GET)
	public String getUserComments(Model model, 
						@RequestParam(value = "userName", required = true) String userName,
						@RequestParam(value = "pageSizeSearch", required = true) Integer pageSizeSearch,
						@RequestParam(value = "pageNumberSearch", required = true) Integer pageNumberSearch,
						@RequestParam(value = "commentUUID", required = true) String commentUUID) {
		
		System.out.println("userName->" + 
				userName + 
				"<-pageSizeSearch->" + 
				pageSizeSearch + 
				"<-pageNumberSearch->" + 
				pageNumberSearch + "<-commentUUID->" + 
				commentUUID);
		
		List<Comment> userComments = new ArrayList<Comment>();
		if(pageNumberSearch >=0 ){
			userComments = searchCommentsComponent.searchAllUserComments(userName, 
					pageNumberSearch, 
					pageSizeSearch, 
					commentUUID);
		}
		
		for(Comment comm: userComments) {
			try{
				commentUUIDsSet.add(commentUUID + comm.getId());
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		//convert to json
		ObjectMapper commentsMapper = new ObjectMapper();
		ArrayNode commentsArray = commentsMapper.createArrayNode();
		for(Comment comment: userComments) {
			ObjectNode commentNode = commentsMapper.createObjectNode();
			commentNode.put("id", comment.getId());
			commentNode.put("parentid", comment.getParentId());
			commentNode.put("userName", comment.getUserName());
			commentNode.put("messageText", comment.getMessageText());
			
			commentsArray.add(commentNode);
		}
		
		return commentsArray.toString();
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value="getcommentscontainstext.htm",method = GET)
	public String getCommentsContainsText(Model model, 
									@RequestParam(value = "predeterminedtext", required = true) String predeterminedText, 
									@RequestParam(value = "pageSizeSearch", required = true) Integer pageSizeSearch,
									@RequestParam(value = "pageNumberSearch", required = true) Integer pageNumberSearch,
									@RequestParam(value = "commentUUID", required = true) String commentUUID) {
		
		System.out.println("predeterminedtext->" + 
		predeterminedText + 
		"<-pageSizeSearch->" + 
		pageSizeSearch + 
		"<-pageNumberSearch->" + 
		pageNumberSearch + "<-commentUUID->" + 
		commentUUID);
		
		List<Comment> commentsWithText = new ArrayList<Comment>();
		if(pageNumberSearch >=0 ){
			commentsWithText = searchCommentsComponent.searchTextInComments(predeterminedText, 
					pageNumberSearch, 
					pageSizeSearch, 
					commentUUID);
		}
		
		for(Comment comm:commentsWithText) {
			try{
				commentUUIDsSet.add(commentUUID + comm.getId());
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		//convert to json
		ObjectMapper commentsMapper = new ObjectMapper();
		ArrayNode commentsArray = commentsMapper.createArrayNode();
		for(Comment comment: commentsWithText) {
			ObjectNode commentNode = commentsMapper.createObjectNode();
			commentNode.put("id", comment.getId());
			commentNode.put("parentid", comment.getParentId());
			commentNode.put("userName", comment.getUserName());
			commentNode.put("messageText", comment.getMessageText());
			
			commentsArray.add(commentNode);
		}
		
		
		return commentsArray.toString();
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	@RequestMapping(value="getdialogpage.htm",method = GET)
	public String getDialodPage(Model model, 
									@RequestParam(value = "pageSizeDialog", required = true) Integer pageSizeDialog,
									@RequestParam(value = "pageNumberDialog", required = true) Integer pageNumberDialog,
									@RequestParam(value = "commentUUIDDialog", required = true) String commentUUIDDialog){
		
		System.out.println("Dialog page->" + 
							":pageSize:" + pageSizeDialog + 
							":pageNumber:" + pageNumberDialog + 
							":commentUUIDDialog:" + commentUUIDDialog);
		
		List<Comment> commentsPageDialog = new ArrayList<Comment>();
		commentsPageDialog = searchCommentsComponent.getBranchPage(pageNumberDialog, pageSizeDialog, commentUUIDDialog);
		
		ObjectMapper commentsMapper = new ObjectMapper();
		ArrayNode commentsArray = commentsMapper.createArrayNode();
		for(Comment comment: commentsPageDialog) {
			ObjectNode commentNode = commentsMapper.createObjectNode();
			commentNode.put("id", comment.getId());
			commentNode.put("parentid", comment.getParentId());
			commentNode.put("userName", comment.getUserName());
			commentNode.put("messageText", comment.getMessageText());
			
			commentsArray.add(commentNode);
		}
		
		return commentsArray.toString();
	}
}
