package testlabinvent.web;

import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.springframework.web.bind.annotation.CrossOrigin;


@Controller
@RequestMapping("/index.htm")
public class IndexController {
	
	@RequestMapping(method = GET)
	 public String welcome(Model model) {
		
		    return "index.html"; //"testlabinvent.html"
		    
	}
	
	@CrossOrigin
	@RequestMapping(method = POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	@ResponseBody
	public String sendUserId(Model model) {
		
		UUID uuid = UUID.randomUUID();
		
		return uuid.toString();
	}
	
	
}
