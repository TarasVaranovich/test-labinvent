package testlabinvent.usecases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import testlabinvent.data.model.Comment;
import testlabinvent.data.services.CommentService;
import testlabinvent.data_redis.model.CommentsCache;
import testlabinvent.data_redis.services.CommentsCacheService;

@Component
@Scope("prototype")
public class SearchCommentsComponent {

	@Autowired
	CommentService commentService;
	
	@Autowired
	CommentsCacheService commentsCacheService;
	
	@Transactional
	public List<Comment> searchAllUserComments(String userName, 
												Integer pageNumber, 
												Integer pageSize, 
												String commentKeyUUID) {
		
		List<Comment> userComments = commentService.getCommentsByUserName(userName, pageNumber, pageSize);
		for(Comment comm: userComments){
			this.createCommentBranchCache(comm.getId(), commentKeyUUID);
		}
		
		return userComments;
	}
	
	@Transactional
	public List<Comment> searchTextInComments(String requiredTextString,
												Integer pageNumber, 
												Integer pageSize, 
												String commentKeyUUID) {
		
		List<Comment> commentsWithText = commentService.getCommentsContainingText(requiredTextString, pageNumber, pageSize);
		
		for(Comment comm: commentsWithText){
			this.createCommentBranchCache(comm.getId(), commentKeyUUID);
		}
		
		return commentsWithText;
	}
	
	public void clearCache(String commentUUID){
		
		commentsCacheService.deleteCommentsCacheById(commentUUID);
		System.out.println("Removed comment chache with Id:" + commentUUID);
		
	}
	
	private void createCommentBranchCache(Integer commentId, String commentKeyUUID){
		
		Comment presettedComm = commentService.getCommentById(commentId);
		Integer topParentId = this.getTopParentId(presettedComm);
		List<Comment> branchComments = new ArrayList<Comment>();
		if(topParentId != null){
			
			branchComments = this.getResultBranch(topParentId);
			
		} 
		String searchedCommentID = commentKeyUUID + commentId;
		this.saveResultBranch(branchComments, presettedComm.getId(), searchedCommentID);
	}
	
	//METHODS FOR NAVIGATION
	private Integer getTopParentId(Comment comment){
		
		Integer topParentId = 0;
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(comment);
		
		try{
			
			while(!commentsQueue.isEmpty()) {
				
				Comment currentComment = commentsQueue.remove();
				Comment parentComment = commentService.getCommentById(currentComment.getParentId());
				if(parentComment != null){
					commentsQueue.add(parentComment);
					resultComments.add(parentComment);
				}
				
			}
			
		} catch(Exception e) {
			
			System.out.println("Exception occured by recieving top parent id...");
			e.printStackTrace();
		}
		
		try{
	
			if(resultComments.size() > 0){
				
				topParentId = resultComments.get(resultComments.size() - 1).getId();
				
			} else {
				
				topParentId = null;
			}		
			
		} catch (Exception e) {
			
			System.out.println("Exception occured by getting top parent id in comment with id:" + comment.getId());
			e.printStackTrace();
		}
		
		return topParentId;
	}
	
	private List<Comment> getResultBranch(Integer topParentId){
		
		List<Comment> topParentChilds = new ArrayList<Comment>();
		topParentChilds = this.getAllChilds(topParentId);
		topParentChilds = this.sortInTreeOrder(topParentChilds);
		
		return topParentChilds;
	}
	
	private void saveResultBranch(List<Comment> commentsForSaveSorted, Integer searchedCommentId, String searchedCommentID){
		List<Integer> commentsIds = new ArrayList<Integer>();
		for(Comment comm: commentsForSaveSorted) {
			commentsIds.add(comm.getId());
		}
		commentsCacheService.saveCommentsCache(new CommentsCache(searchedCommentID, commentsIds,  searchedCommentId));
	}
	
	//FETCH ALL BRANCH WHOLLY, BECAUSE COMMENTS BRANCH WILL RESTRICTED (IT'S NOT BIG IN COMPARISON WITH ALL COMMENTS COUNT)
	@Transactional
	public List<Comment> getBranchPage(Integer pageNumber, Integer pageSize, String searchedCommentID){
		
		System.out.println("Search component-recieving branch page->" + 
							":pageNumber:" + pageNumber + 
							":pageSize:" + pageSize + 
							":searchedCommentId:" + searchedCommentID);
		List<Comment> commentsPage = new ArrayList<Comment>();
		List<Integer> commentIds = new ArrayList<Integer>();
		try{
			
			commentIds = commentsCacheService.getCommentsCacheById(searchedCommentID).getChildsIDs();
			System.out.println("List of cached comments id's size: " +commentIds.size());
			
		} catch(Exception  e){
			
			System.out.println("Error recieving commet id's from cache...");
		}
		
		if(commentIds.size() > 0){// is comment have cache?
					
			List<Comment> allBranchComments = new ArrayList<Comment>();
			try{
				
				allBranchComments = commentService.getCommentsByIds(commentIds);
			} catch(Exception e){
				
				System.out.println("Error recieving comments from Database by existing id's...");
			}
			try{
				
				allBranchComments = this.sortCommentsInIdsListOrder(allBranchComments, commentIds);
				
			} catch(Exception e) {
				
				System.out.println("Error sorting in tree order....");
			}
						
			Integer searchedCommentId = commentsCacheService.getCommentsCacheById(searchedCommentID).getCommentParent();
			
			Comment searchedComment = null;
			
			try{
				
				searchedComment = commentService.getCommentById(searchedCommentId);
			} catch(Exception e) {
				
				System.out.println("Error recieving serched comment from database by it's id...");
			}
			Comment commInList = null;
			try {
				
				commInList = allBranchComments.stream().filter(item -> Objects.equals(item.getId(), searchedCommentId)).collect(Collectors.toList()).get(0);
				
			} catch(Exception e) {
				
				System.out.println("Error recieving first comment for getting it's position...");
				System.out.println("List of comments witch search was performing: " + allBranchComments.size());
			}
			
			Integer commentPosition = allBranchComments.indexOf(commInList);
			//for debug only
			System.out.println("Comment position:" + commentPosition + "-comment id:" + commInList.getId());
			System.out.print("List of cached comments:");
			for(Comment comm: allBranchComments){
				System.out.print(comm.getId() + "|");
			}
			System.out.println("");
			//end for debug only
			try{
				if(pageNumber > 0) {
					
					Integer lastPosition = commentPosition + pageSize*pageNumber;
					//CHECK IF INDEX OUT OF BOUND OF LIST
					if(lastPosition > allBranchComments.size() - 1) {
						
						lastPosition = allBranchComments.size(); // -1
					}
					System.out.println("Sublist first:" +(commentPosition + pageSize*(pageNumber - 1)) + "-Sublist last:" + lastPosition);
					
					commentsPage = allBranchComments.subList(commentPosition + pageSize*(pageNumber - 1),lastPosition);
					
					System.out.println("Sublist result:" + commentsPage.size());
					
				} else if(pageNumber < 0){
					
					pageNumber = Math.abs(pageNumber);
					Integer firstPosition  = commentPosition - pageSize*pageNumber;
					//CHECK IF INDEX OUT OF BOUND OF LIST
					if(firstPosition < 0){
						
						firstPosition = 0;
					}
					
					commentsPage = allBranchComments.subList(firstPosition, commentPosition - pageSize*(pageNumber - 1));
								
				} else {
					
					//NOTHING TO DO
				}
			} catch(Exception e) {
				
				System.out.println("Problems getting sublist(page)");
			}
		}
		
			
		return commentsPage;
	}
	//END METHODS FOR NAVIGATION
	
	//ADOPTED METHODS FROM OTHER CLASSES
	private List<Comment> getAllChilds(Integer parentCommentId){
		
		Comment predeterminedComment = commentService.getCommentById(parentCommentId);
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(predeterminedComment);
		while(!commentsQueue.isEmpty()) {
			Comment currentComment = commentsQueue.remove();
			List<Comment> childs = commentService.getCommentChilds(currentComment.getId());
			for(Comment child: childs) {
				commentsQueue.add(child);
				resultComments.add(child);
			}
		}
		
		return resultComments;
	}
	
	private List<Comment> sortInTreeOrder(List<Comment> selectedComments){
		//COMPARE_BY_PARENTID
		Collections.sort(selectedComments, Comment.COMPARE_BY_PARENTID);
		List<Comment> commentsBuff = new ArrayList<Comment>();
		List<Comment> commentsResult = new ArrayList<Comment>();
		commentsResult.addAll(selectedComments);
		for(Comment comm: selectedComments) {
			commentsBuff.clear();
			
			for(Comment comm2: selectedComments) {
				if(comm2.getParentId() == comm.getId()){
					commentsBuff.add(comm2);
				}
			}
			if(commentsBuff.isEmpty() == false) {
				
				commentsResult.removeAll(commentsBuff);
				Collections.reverse(commentsBuff);
				Collections.reverse(commentsResult);
				commentsResult.addAll(commentsResult.indexOf(comm), commentsBuff);	
				Collections.reverse(commentsResult);
				
			}
		}
		
		return commentsResult;
	}
	
	private List<Comment> sortCommentsInIdsListOrder(List<Comment> commentsList, List<Integer> indexesList) {
		
		List<Comment> sortedList = new ArrayList<Comment>();

		for(Integer index: indexesList) {
			Comment comm = commentsList.stream().filter(item -> Objects.equals(item.getId(), index)).collect(Collectors.toList()).get(0);
			sortedList.add(comm);
		}	
		
		return sortedList;
	}
	//END ADOPTED METHODS FROM OTHER CLASSES
}
