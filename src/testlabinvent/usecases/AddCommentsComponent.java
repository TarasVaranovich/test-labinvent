package testlabinvent.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import testlabinvent.data.services.CommentService;
import testlabinvent.data.model.Comment;

@Component
@Scope("prototype")
public class AddCommentsComponent {
	
	@Autowired
	CommentService commentService;
	
	@Transactional
	public boolean addComment(Comment comment) {
		
		return commentService.addComment(comment.getMessageText(), comment.getUserName(),comment.getParentId());
	}
}
