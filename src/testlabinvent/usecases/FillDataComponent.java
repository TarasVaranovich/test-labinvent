package testlabinvent.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

import testlabinvent.data.services.CommentService;

@Component
@Scope("singleton")
public class FillDataComponent {
	
	@Autowired(required = true)
	CommentService commentService;
	
	@Transactional
	public void fillData() {
		//"John" "David" "Thomas" "Donald" "Paul" , "Mary" "Nancy" "Susan" "Ruth" "Jennifer"
		if(commentService == null) {
			
			System.out.println("Comment service is not wired to FillDataComponent");
			
		} 
		System.out.println("Filling test data...");
		commentService.addComment("text bla-bla", "John", 0);//1
		commentService.addComment("text", "Thomas", 1);//2
		commentService.addComment("text", "Mary", 1);//3
		commentService.addComment("text", "David", 0);//4
		commentService.addComment("text", "John", 2);//5
		commentService.addComment("Megatron-3000", "George", 0);//6
		commentService.addComment("text", "Thomas", 5);//7
		commentService.addComment("He is anonimous", "David", 7);//8
		commentService.addComment("text", "Betty", 1);//9
		commentService.addComment("text", "Thomas", 6);//10
		commentService.addComment("next_process...", "Susan", 9);//11
		commentService.addComment("text", "Nancy", 1);//12
		commentService.addComment("text", "Ruth", 12);//13
		commentService.addComment("text", "John", 2);//14
		commentService.addComment("text", "Donald", 10);//15
		commentService.addComment("text", "David", 10);//16
		//--
		commentService.addComment("text", "Mary", 0);//17
		commentService.addComment("text", "Mary", 17);//18
		commentService.addComment("text", "Nancy", 18);//19
		commentService.addComment("text", "Susan", 17);//20
		commentService.addComment("Megatron-3000", "Donald", 18);//21
		commentService.addComment("text", "Susan", 19);//22
		commentService.addComment("text", "Ruth", 19);//23
		commentService.addComment("text", "Donald", 20);//24
		commentService.addComment("Megatron-3000", "Thomas", 21);//25
		commentService.addComment("text", "Mary", 20);//26
		commentService.addComment("text", "Jennifer", 17);//27
		commentService.addComment("text", "Ruth", 20);//28
		commentService.addComment("text", "Paul", 28);//29
		commentService.addComment("$Min%/max", "Mary", 27);//30
		commentService.addComment("text", "Jennifer", 26);//31
		commentService.addComment("text", "David", 27);//32
		commentService.addComment("text", "Paul", 0);//33
		commentService.addComment("text", "Susan", 0);//34
		commentService.addComment("text", "Ruth", 0);//35
		commentService.addComment("text", "Mark", 35);//36
		commentService.addComment("text", "Donald", 35);//37
		commentService.addComment("text", "Susan", 35);//38
		commentService.addComment("12345 ....???", "Marry", 36);//39
		commentService.addComment("text", "Jennifer", 35);//40
		commentService.addComment("text", "Mark", 37);//41
		commentService.addComment("text", "John", 36);//42
		commentService.addComment("text", "Ruth", 40);//43
		commentService.addComment("text", "Nancy", 35);//44
		commentService.addComment("text", "Marry", 37);//45
		commentService.addComment("This song is pretty good, but musician is bad guy...", "Donald", 36);//46
		commentService.addComment("text", "Jennifer", 37);//47
		commentService.addComment("text", "Jennifer", 43);//48
		commentService.addComment("\tNot \tfor \tme ", "Ruth", 48);//49
		commentService.addComment("\nBut \nfor \nhes frends", "Jennifer", 49);//50	
		commentService.addComment("text","Jennifer", 0);//51	
		commentService.addComment("text", "Nancy", 0);//52	
		commentService.addComment("text", "Mark", 0);//53	
		commentService.addComment("text", "Betty", 0);//54
			
	}
}
