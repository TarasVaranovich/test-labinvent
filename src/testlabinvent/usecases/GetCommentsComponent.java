package testlabinvent.usecases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import testlabinvent.data.model.Comment;
import testlabinvent.data.services.CommentService;
import testlabinvent.data_redis.model.CommentsCache;
import testlabinvent.data_redis.services.CommentsCacheService;

@Component
@Scope("prototype")
public class GetCommentsComponent {

	@Autowired
	CommentService commentService;
	
	@Autowired
	CommentsCacheService commentsCacheService;
	
	@Transactional
	public List<Comment> getSpecificPage(String sessionUUID, Integer pageSize , Integer pageNumber, String pageKey) {
		
		List<Comment> specificPageComments = new ArrayList<Comment>();	
	
		if(this.getPageData(pageKey + pageNumber).size() == 0) {	
			//clear chache
			try {
				
				commentsCacheService.deleteCommentsCacheById(sessionUUID);
				
			} catch(Exception e) {
				
				System.out.println("Problem deleting cache:");
				e.printStackTrace();
				
			}
			System.out.println("Getting page from database:" + pageNumber);
			for(int i = 0; i < pageNumber; i++){
				specificPageComments.clear();
				specificPageComments = this.fetchNextComment(pageNumber, pageSize, sessionUUID);
				while(specificPageComments.size() < pageSize) {
					
					List<Comment> restOfComments = this.fetchNextComment(pageNumber, pageSize - specificPageComments.size(), sessionUUID);
					
					if(restOfComments.size() == 0) {
						
						break;
					} else {
						
						specificPageComments.addAll(restOfComments);
					}				
				}
				this.savePageData(specificPageComments, pageKey + pageNumber);
			}
			
		} else {
			System.out.println("Getting page from cache: " + pageKey + pageNumber);
			
			try{
				
				specificPageComments = this.getPageData(pageKey + pageNumber);
				
			} catch(Exception e) {
				
				System.out.println("Problem getting page from cache...");
				e.printStackTrace();
				
			}
		}
					
		return specificPageComments;
	}
	
	public void updatePagesCache(String sessionUUID, String pageKey) {
		System.out.println("Updating cache....");
		Integer lastPageNumber = 1;
		Integer pageSize = 0;
		//MUST RESEACH BETTER DECISION (IT'S TUOCH lastPageNumber AND LOOP)
		while(commentsCacheService.getCommentsCacheById(pageKey + lastPageNumber) != null) {
			
			if(commentsCacheService.getCommentsCacheById(pageKey + lastPageNumber).getChildsIDs().size() > pageSize) {
				
				pageSize = commentsCacheService.getCommentsCacheById(pageKey + lastPageNumber).getChildsIDs().size();
				
				
			}
			commentsCacheService.deleteCommentsCacheById(pageKey + lastPageNumber);	
			/*test*/
			if(!sessionUUID.equals("clear")){
				this.getSpecificPage(sessionUUID, pageSize, lastPageNumber, pageKey);
			}
			/*end test*/
			lastPageNumber++;
			
		} 
		
		if(!sessionUUID.equals("clear")){ // this is branch if we want clear pages cache, not update
			
			System.out.println("Page size:" + pageSize + "-Last page number:" + lastPageNumber + "-SessionUUID:" + sessionUUID);
			//this.getSpecificPage(sessionUUID, pageSize, lastPageNumber, pageKey);
		} else {
			
			System.out.println("Cleaning pages cache after application stop complete.");
		}
		
		System.out.println("Updating cache complete:" + System.currentTimeMillis());		
	}
	
	private List<Comment> fetchNextComment(Integer pageNumber, Integer pageSize, String sessionUUID) {
		
		List<Comment> resultCommentsPage = new ArrayList<Comment>();
		Integer currentRootId = 0;
		List<Integer> childsInCache = new ArrayList();
		
		if(commentsCacheService.getCommentsCacheById(sessionUUID) == null) {
			
			currentRootId = this.fetchNextTopId(0);
			
			//Duplicate!!!!
			resultCommentsPage.add(commentService.getCommentById(currentRootId));
			resultCommentsPage.addAll(this.getAllChilds(currentRootId));
			resultCommentsPage = this.sortInTreeOrder(resultCommentsPage);
			
			
			if(resultCommentsPage.size() > pageSize) {
				
				childsInCache = this.saveRestInCache(resultCommentsPage, pageSize);
				resultCommentsPage = resultCommentsPage.subList(0, pageSize);
				
			}
			
		} else if(commentsCacheService.getCommentsCacheById(sessionUUID).getChildsIDs().size() > 0) {
			
			List<Comment> fetchedComments = new ArrayList<Comment>();
			List<Integer> cachedIds = new ArrayList<Integer>();
			Map<String, List<?>> resultMap = new HashMap<String, List<?>>();
			resultMap = this.fetchCommentsFromCache(sessionUUID, pageSize);
			List<?> fetchedCommentsGen = this.fetchCommentsFromCache(sessionUUID, pageSize).get("fetchedComments");
			List<?> cachedIdsGen = this.fetchCommentsFromCache(sessionUUID, pageSize).get("cachedIds");
			fetchedComments = (List<Comment>)(Object)fetchedCommentsGen;
			cachedIds = (List<Integer>)(Object)cachedIdsGen;
			childsInCache = cachedIds;
			resultCommentsPage = fetchedComments;
			currentRootId = commentsCacheService.getCommentsCacheById(sessionUUID).getCommentParent();
			
		} else {
					
			currentRootId = this.fetchNextTopId(commentsCacheService.getCommentsCacheById(sessionUUID).getCommentParent());
			 //Duplicate!!!!
			if(currentRootId != null) {
				
				resultCommentsPage.add(commentService.getCommentById(currentRootId));
				resultCommentsPage.addAll(this.getAllChilds(currentRootId));
				resultCommentsPage = this.sortInTreeOrder(resultCommentsPage);
				
			} else {
				
				resultCommentsPage.clear();
				
			}	
			
			if(resultCommentsPage.size() > pageSize) {
				
				childsInCache = this.saveRestInCache(resultCommentsPage, pageSize);
				resultCommentsPage = resultCommentsPage.subList(0, pageSize);
				
			}
			
		}
		
		if(currentRootId != null) {
			
			commentsCacheService.saveCommentsCache(new CommentsCache(sessionUUID, childsInCache, currentRootId));
			
		}
			
		return resultCommentsPage;
	}
	
	private List<Integer> saveRestInCache(List<Comment> oversizePage, Integer pageSize) {
		
		List<Integer> cachedIds = new ArrayList<Integer>();
		oversizePage = oversizePage.subList(pageSize, oversizePage.size());
		
		for(Comment comm: oversizePage) {
			
			cachedIds.add(comm.getId());
			
		}
		
		return cachedIds;
	}
	
	private Map<String, List<?>> fetchCommentsFromCache(String sessionUUID, Integer pageSize) {
		
		List<Comment> fetchedComments = new ArrayList<Comment>();
		List<Integer> fetchedIds = new ArrayList<Integer>();
		fetchedIds = commentsCacheService.getCommentsCacheById(sessionUUID).getChildsIDs();
		fetchedComments = commentService.getCommentsByIds(fetchedIds);
		fetchedComments = this.sortCommentsInIdsListOrder(fetchedComments, fetchedIds);
		fetchedIds.clear();
		
		if(fetchedComments.size() > pageSize) {
			
			fetchedIds = this.saveRestInCache(fetchedComments, pageSize);
			fetchedComments = fetchedComments.subList(0, pageSize);
			
		} 
		HashMap<String,  List<?>> result = new HashMap<String, List<?>>();
		result.put("fetchedComments", fetchedComments);
		result.put("cachedIds", fetchedIds);
		
		return result;
	}
	
	private Integer fetchNextTopId(Integer previousId) {
		
		SortedSet<Integer> topComments = new TreeSet<Integer>();
		Integer topParentId  = commentService.getTopParentId();
		topComments.addAll(commentService.getCommentIdsByParentId(topParentId));
		
		if(previousId == 0) {
						
			return topComments.first();
			
		} else {
			
			Iterator setIterator = topComments.tailSet(previousId).iterator();
			setIterator.next();
			
			if(topComments.tailSet(previousId).size() > 1) {
				
				return (Integer)setIterator.next();
				
			} else {
				
				return null;
			}		
		}		
	}
	
	private void savePageData(List<Comment> pageComments, String pageId) {
		List<Integer> commentsIds = new ArrayList<Integer>();
		for(Comment comm: pageComments) {
			
			commentsIds.add(comm.getId());
			
		}
		CommentsCache commentsCache = new CommentsCache(pageId,commentsIds, null);
		commentsCacheService.saveCommentsCache(commentsCache);
	}
	
	private List<Comment> getPageData(String pageId) {
		System.out.println("Getting page data...");
		List<Comment> pageData = new ArrayList<Comment>();
		List<Integer> cachedIds = new ArrayList<Integer>();
		
		if(commentsCacheService.getCommentsCacheById(pageId) != null) {
			
			cachedIds = commentsCacheService.getCommentsCacheById(pageId).getChildsIDs();
			pageData = commentService.getCommentsByIds(cachedIds); // ???
			
			try {
				
				pageData = this.sortCommentsInIdsListOrder(pageData, cachedIds);
				
			} catch (Exception e) {
				System.out.println("");
				
				for(Comment comm: pageData) {	
					
					System.out.print(comm.getId() + ":");
				}
				
				System.out.println("Problem with sort in tree order:"  + cachedIds.toString());				
			}
		} 
		
		return pageData;
	}
	
	private List<Comment> sortInTreeOrder(List<Comment> selectedComments){
		
		Collections.sort(selectedComments, Comment.COMPARE_BY_PARENTID);
		List<Comment> commentsBuff = new ArrayList<Comment>();
		List<Comment> commentsResult = new ArrayList<Comment>();
		commentsResult.addAll(selectedComments);
		for(Comment comm: selectedComments) {
			commentsBuff.clear();
			
			for(Comment comm2: selectedComments) {
				if(comm2.getParentId() == comm.getId()){
					commentsBuff.add(comm2);
				}
			}
			if(commentsBuff.isEmpty() == false) {
				
				commentsResult.removeAll(commentsBuff);
				Collections.reverse(commentsBuff);
				Collections.reverse(commentsResult);
				commentsResult.addAll(commentsResult.indexOf(comm), commentsBuff);	
				Collections.reverse(commentsResult);
				
			}
		}
		
		return commentsResult;
	}
	
	private List<Comment> getAllChilds(Integer parentCommentId){
		
		Comment predeterminedComment = commentService.getCommentById(parentCommentId);
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(predeterminedComment);
		while(!commentsQueue.isEmpty()) {
			Comment currentComment = commentsQueue.remove();
			List<Comment> childs = commentService.getCommentChilds(currentComment.getId());
			for(Comment child: childs) {
				commentsQueue.add(child);
				resultComments.add(child);
			}
		}
		
		return resultComments;
	}
	
	private List<Comment> sortCommentsInIdsListOrder(List<Comment> commentsList, List<Integer> indexesList) {
		
		List<Comment> sortedList = new ArrayList<Comment>();

		for(Integer index: indexesList) {
			Comment comm = commentsList.stream().filter(item -> Objects.equals(item.getId(), index)).collect(Collectors.toList()).get(0);
			sortedList.add(comm);
		}	
		
		return sortedList;
	}
	
}
