package testlabinvent.data.model;


import java.util.Comparator;

import javax.persistence.*;


@Entity
@Table(name="comment")
public class Comment {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="comment_id", nullable=false)
    private Integer id;
	@Column(name="messagetext", nullable=true)
	private String messageText;
	@Column(name="username", nullable=false)
	private String userName;
	@Column(name="parentID", nullable=false)
	private Integer parentID;
	
	public Comment() {

		this.messageText = null;
		this.userName = "default";
		this.parentID = 0;
	
	}
	
	public Comment(String messageText, 
				  String userName, 
				  Integer parentID) {
		
		this.messageText = messageText;
		this.userName = userName;
		this.parentID = parentID;
		
	}	
	
	public Integer getId() {
		
		return this.id;
		
	}
	
	public String getMessageText() {
		
		return this.messageText;
		
	}
	
	public String getUserName() {
		
		return this.userName;
		
	}
	
	public Integer getParentId() {
		
		return this.parentID;
		
	}
	
	public static final Comparator<Comment> COMPARE_BY_PARENTID = new Comparator<Comment>() {
		
        @Override
        public int compare(Comment lhs, Comment rhs) {
        	
            return lhs.getParentId() - rhs.getParentId();
            
        }
    };
	
}
