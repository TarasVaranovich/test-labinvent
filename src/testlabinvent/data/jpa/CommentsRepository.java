package testlabinvent.data.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import testlabinvent.data.model.Comment;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

@Repository
public interface CommentsRepository extends CrudRepository<Comment, Long> {

	Page<Comment> findAll(Pageable pageable);
	Page<Comment> findAllByuserName(String userName, Pageable pageable);
	List<Comment> findAllByparentID(Integer commentID);
	@Query(value = "select parentID from comment order by parentID asc limit 1" , nativeQuery = true)
	Integer getTopParentId();
	List<Comment> findAllByIdIn(List<Integer> ids);
	@Query(value = "select comment_id from comment where comment.parentID = :commentID" , nativeQuery = true)
	List<Integer> getIdsByparentID(@Param("commentID")Integer commentID);
	Comment findById(Integer commentID);
	@Query(value = "select * from comment where comment.messagetext like :wantedText limit :limitbegin, :limitsize" , nativeQuery = true)
	List<Comment> findTextInComments(@Param("wantedText")String wantedText, 
									@Param("limitbegin") Integer limitBegin, 
									@Param("limitsize") Integer limitSize);
}
