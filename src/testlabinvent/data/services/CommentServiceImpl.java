package testlabinvent.data.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import testlabinvent.data.jpa.CommentsRepository;
import testlabinvent.data.model.Comment;

@Service
@Scope
public class CommentServiceImpl implements CommentService{
	
	private final CommentsRepository commentsRepository;
	
	@Autowired(required = true)
    public  CommentServiceImpl(CommentsRepository commentsRepository){

        this.commentsRepository = commentsRepository;

    }
	
	@Override //IS NOT SIMPLE, BECAUSE MUST GET ALL WITH "0"-PARENT WITH CHILDS
	public List<Comment> getCommentsPage(Integer pageNumber, Integer pageSize){
		
		 Pageable pageable = new PageRequest(pageNumber,pageSize);
		 List<Comment> commentsSelected = new ArrayList<>();
		 
		 for (Comment comm: commentsRepository.findAll(pageable)) {

			 commentsSelected.add(comm);

	     }
		 
		 return commentsSelected;
	}
	
	@Override
	public List<Comment> getCommentsByUserName(String userName, Integer pageNumber, Integer pageSize){
		
		Pageable pageable = new PageRequest(pageNumber,pageSize);
		List<Comment> commentsSelected = new ArrayList<>();
		
		for (Comment comm: commentsRepository.findAllByuserName(userName,pageable)) {

			 commentsSelected.add(comm);

	     }
		
		return commentsSelected;
		
	}
	
	@Override
	public List<Comment> getCommentChilds(Integer commentID){
		
		return commentsRepository.findAllByparentID(commentID);
		
	}
	
	@Override
	public boolean addComment(String commentText, String userName, Integer parentID){
		
		Comment comment = new Comment(commentText, userName, parentID);
		
		try{
			
			commentsRepository.save(comment);
			
			return true;
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
			return false;
			
		}
		
	}
	
	@Override
	public Integer getTopParentId(){
		
		return commentsRepository.getTopParentId();
	}
	
	@Override
	public List<Comment> getCommentsByIds(List<Integer> idsList) {
		
		return commentsRepository.findAllByIdIn(idsList);
	}
	
	@Override
	public List<Integer> getCommentIdsByParentId(Integer parentId){
		
		return commentsRepository.getIdsByparentID(parentId);
	}
	
	@Override
	public Comment getCommentById(Integer commentId) {
		
		return commentsRepository.findById(commentId);
	}
	
	@Override
	public List<Comment> getCommentsContainingText(String requiredText, Integer pageNumber, Integer pageSize) {
		Pageable pageable = new PageRequest(pageNumber,pageSize);
		List<Comment> commentsSelected = new ArrayList<>();
		
		return commentsRepository.findTextInComments(requiredText, pageSize * pageNumber, pageSize);
	}
}
