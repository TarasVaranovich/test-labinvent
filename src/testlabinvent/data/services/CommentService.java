package testlabinvent.data.services;

import java.util.List;
import java.util.Set;

import testlabinvent.data.model.Comment;

public interface CommentService {
	//because we store comments in order of adding and get orders in order of adding
	//from database
	List<Comment> getCommentsPage(Integer pageNumber, Integer pageSize);
	List<Comment> getCommentsByUserName(String userName, Integer pageNumber, Integer pageSize);
	List<Comment> getCommentChilds(Integer commentID);
	boolean addComment(String commentText, String userName, Integer parentID);
	Integer getTopParentId();
	List<Comment> getCommentsByIds(List<Integer> idsSet);
	List<Integer> getCommentIdsByParentId(Integer parentId);
	Comment getCommentById(Integer commentId);
	List<Comment> getCommentsContainingText(String requiredText, Integer pageNumber, Integer pageSize);
}
