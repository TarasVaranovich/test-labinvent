package testlabinvent.math;

import java.util.Comparator;


public class Value {
	
	private Integer ID;
	private Integer parentID;
	private boolean existenceMarker;
	
	Value (Integer ID, Integer parentID) {
		
		this.ID = ID;
		this.parentID = parentID;
		this.existenceMarker = true;
	}
	
	public Integer getID(){
		
		return this.ID;
		
	}
	
	public Integer getparentID(){
		
		return this.parentID;
		
	}
	
	public Boolean getexistenceMarker(){
		
		return this.existenceMarker;
		
	}
	
	public void setexistenceMarker(Boolean existenceMarker) {
		
		this.existenceMarker = existenceMarker;
		
	}
	
	public static final Comparator<Value> COMPARE_BY_PARENT = new Comparator<Value>() {
		
        @Override
        public int compare(Value lhs, Value rhs) {
        	
            return lhs.getparentID() - rhs.getparentID();
            
        }
    };
	
}

