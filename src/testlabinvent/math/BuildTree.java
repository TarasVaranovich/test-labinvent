package testlabinvent.math;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import testlabinvent.math.Value;

public class BuildTree {
	
	public static void main(String[] args) { 
		
		//Value v0 = new Value(0,0);
		Value v1 = new Value(1,0);
		Value v2 = new Value(2,1);
		Value v3 = new Value(3,1);
		Value v4 = new Value(4,0);
		Value v5 = new Value(5,2);
		Value v6 = new Value(6,0);
		Value v7 = new Value(7,5);
		Value v8 = new Value(8,7);
		Value v9 = new Value(9,1);
		Value v10 = new Value(10,6);
		Value v11 = new Value(11,9);
		Value v12 = new Value(12,1);
		Value v13 = new Value(13,12);
		Value v14 = new Value(14,2);
		Value v15 = new Value(15,10);
		Value v16 = new Value(16,10);
		
		List<Value> values = new ArrayList<Value>();
		
		//values.add(v0); // VALUE CANT BE PAREANT ITSELF
		values.add(v1);
		values.add(v2);
		values.add(v3);
		values.add(v4);
		values.add(v5);
		values.add(v6);
		values.add(v7);
		values.add(v8);
		values.add(v9);
		values.add(v10);
		values.add(v11);
		values.add(v12);
		values.add(v13);
		values.add(v14);
		values.add(v15);
		values.add(v16);
		
		Collections.sort(values, Value.COMPARE_BY_PARENT);
		
		for(Value val: values) {
			
			System.out.println(val.getID() + ":" + val.getparentID());
			
		}
		System.out.println("Build tree...");
		//Value valuesArray[] = values.toArray(new Value[values.size()]);
		List<Value> valBuff = new ArrayList<Value>();
		List<Value> valResult = new ArrayList<Value>();
		valResult.addAll(values);
		//Collections.reverse(values);
		for(Value val: values) {
			valBuff.clear();
			//System.out.println("Empty::" + valBuff.isEmpty());
			for(Value val2: values) {
				if(val2.getparentID() == val.getID()){
					valBuff.add(val2);
				}
			}
			if(valBuff.isEmpty() == false) {
				//delete duplicates
				valResult.removeAll(valBuff);
				//end delete duplicates
				Collections.reverse(valBuff);
				Collections.reverse(valResult);
				valResult.addAll(valResult.indexOf(val), valBuff);	
				Collections.reverse(valResult);
				//System.out.println("New size:" + valResult.size());
			}
		}
		
		System.out.println("Result List...");
		for(Value val3: valResult) {
			System.out.println(val3.getID() + ":" + val3.getparentID());
		}
		
	}
}
