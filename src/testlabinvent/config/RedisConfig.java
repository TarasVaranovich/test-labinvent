package testlabinvent.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@ComponentScan("testlabinvent.data_redis")
@PropertySource("classpath:/testlabinvent_redis.properties")
public class RedisConfig {
	
	@Autowired
	Environment environment;
	
	@Bean(name = "jedisConnectionFactory")
	public JedisConnectionFactory jedisConnectionFactory(){ // method threw exception 
		
		try {
			
			JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
			jedisConnectionFactory.setPort(Integer.parseInt(environment.getProperty("port")));
			jedisConnectionFactory.setHostName(environment.getProperty("host"));
			jedisConnectionFactory.setTimeout(Integer.parseInt(environment.getProperty("timeout")));
			
			return jedisConnectionFactory;
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
			return null;
		}
	}
		 
	@Bean(name = "redisTemplate")
	public RedisTemplate<String, Object> redisTemplate() {
		
		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
		
		try{
			
			redisTemplate.setConnectionFactory(this.jedisConnectionFactory());
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
	    return redisTemplate;
	}
	
}
