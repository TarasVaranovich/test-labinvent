package testlabinvent.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.http.converter.HttpMessageConverter;
//import java.util.List;


@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {"testlabinvent.web"})
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	
	@Bean
    public InternalResourceViewResolver getInternalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
        resolver.setPrefix("/pages/");
        resolver.setSuffix("");
        return resolver;
    }
	
	
	/*@Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		
		System.out.println("Begin register Converters");
		
        converters.add(new MappingJackson2HttpMessageConverter());
        
        super.configureMessageConverters(converters);
        
        System.out.println("End register Converters");
    }*/

}
