package testlabinvent.config;

//import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.WebApplicationInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.servlet.DispatcherServlet;

import testlabinvent.config.WebMvcConfig;
import testlabinvent.config.PersistenceJPAConfig;
import testlabinvent.config.RedisConfig;
import testlabinvent.listeners.ApplicationContextLoadListener;

import javax.servlet.ServletRegistration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class AppInitializer implements WebApplicationInitializer {
	
	@Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        //ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
        // for exception handling
        DispatcherServlet dispatcherServlet = new DispatcherServlet(context);
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);   
        ServletRegistration.Dynamic dispatcher;
        dispatcher = servletContext.addServlet("DispatcherServlet", dispatcherServlet);
        // end for exception handling*/
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("*.htm");
    }

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation("testlabinvent.web");
        context.register(WebMvcConfig.class);
        context.register(PersistenceJPAConfig.class);
        context.register(RedisConfig.class);
        context.register(UsecasesConfig.class);
        context.addApplicationListener(new ApplicationContextLoadListener());
        return context;
    }
 
}
