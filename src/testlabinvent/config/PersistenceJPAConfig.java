package testlabinvent.config;


//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;


@Configuration
@ComponentScan("testlabinvent.data")
@EnableJpaRepositories(basePackages="testlabinvent.data.*")
@EnableTransactionManagement /*checks annotated @transactional beans in app-context, not services */
@PropertySource("classpath:/testlabinvent_rdbms.properties")
public class PersistenceJPAConfig {
		
	   @Autowired
	   Environment environment;
	   
	   @Bean(name = "entityManagerFactory")
	   //@Scope("prototype")
	   public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
	      LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
	      em.setDataSource(dataSource());
	      em.setPackagesToScan("testlabinvent.data.*"); 
	      JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	      em.setJpaVendorAdapter(vendorAdapter);
	      em.setJpaProperties(additionalProperties());

	      return em;
	   }
	 
	   @Bean
	   public DataSource dataSource(){
	      DriverManagerDataSource dataSource = new DriverManagerDataSource();
	      dataSource.setDriverClassName(environment.getProperty("driver"));
	      dataSource.setUrl(environment.getProperty("url"));
	      dataSource.setUsername(environment.getProperty("username"));
	      dataSource.setPassword(environment.getProperty("password"));
	      return dataSource;
	   }
	 
	   @Bean(name = "transactionManager")
	   public PlatformTransactionManager transactionManager(EntityManagerFactory emf){ //bind connection to currently executing thred
	      JpaTransactionManager transactionManager = new JpaTransactionManager();
	      transactionManager.setEntityManagerFactory(emf);

	      return transactionManager;
	   }
	 
	   @Bean
	   public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
	      return new PersistenceExceptionTranslationPostProcessor();
	   }
	 
	   Properties additionalProperties() {
	      Properties properties = new Properties();
	      properties.setProperty("hibernate.hbm2ddl.auto", environment.getProperty("hibernatecreationtype")); //validate, update, create, create-drop
	      properties.setProperty("hibernate.dialect", environment.getProperty("hibernatedialect"));
	      return properties;
	   }
	   
}
