package testlabinvent.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import testlabinvent.usecases.FillDataComponent;
import testlabinvent.usecases.GetCommentsComponent;
import testlabinvent.usecases.SearchCommentsComponent;
import testlabinvent.usecases.AddCommentsComponent;

@Configuration
@ComponentScan("testlabinvent.usecases")
public class UsecasesConfig {
	
	@Bean(name = "fillDatabaseBean")
	public FillDataComponent fillDataComponent() {
		
		return new FillDataComponent();
	}
	
	@Bean(name = "getCommentsBean")
	public GetCommentsComponent getCommentsComponent() {
		
		return new GetCommentsComponent();
	}	
	
	@Bean(name = "searchCommentsBean")
	public SearchCommentsComponent searchCommentsComponent(){
		
		return new SearchCommentsComponent();
	}
	
	@Bean(name = "addCommentsBean")
	public AddCommentsComponent addCommentsComponent() {
		
		return new  AddCommentsComponent();
	}
}
