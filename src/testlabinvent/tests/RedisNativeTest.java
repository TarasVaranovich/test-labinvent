package testlabinvent.tests;

import java.util.Map;

import redis.clients.jedis.Jedis;

import java.lang.Thread;

public class RedisNativeTest {
	
	public static void main(String [] args){
		
	final Jedis jedis = new Jedis("localhost", 6379, 2000);
	
		jedis.hset("dbone", "keyone", "valueone");
		Map<String, String> results = jedis.hgetAll("dbone");
		try {
			Thread.sleep(1000);
			System.out.println(results);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
