package testlabinvent.tests;

import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import testlabinvent.config.PersistenceJPAConfig;
import testlabinvent.data.services.CommentService;
import testlabinvent.data.model.Comment;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class GetCommentsTreePageSeparateMethodsTest {
	
	@Autowired
	CommentService commentService;
	
	@Test
	public void getCommentsTreeTest(){
		
		System.out.println("Filling test data...");
		commentService.addComment("text", "user", 0);//1
		commentService.addComment("text", "user", 1);//2
		commentService.addComment("text", "user", 1);//3
		commentService.addComment("text", "user", 0);//4
		commentService.addComment("text", "user", 2);//5
		commentService.addComment("text", "user", 0);//6
		commentService.addComment("text", "user", 5);//7
		commentService.addComment("text", "user", 7);//8
		commentService.addComment("text", "user", 1);//9
		commentService.addComment("text", "user", 6);//10
		commentService.addComment("text", "user", 9);//11
		commentService.addComment("text", "user", 1);//12
		commentService.addComment("text", "user", 12);//13
		commentService.addComment("text", "user", 2);//14
		commentService.addComment("text", "user", 10);//15
		commentService.addComment("text", "user", 10);//16
		
		System.out.println("Get added comments...");
		for(Comment comm: commentService.getCommentsPage(0, 20)) {
			System.out.println(comm.getId() + ":" + comm.getParentId());
		}
		
		System.out.println("Get top parentId...");
		
		System.out.println("Top parent id:" + commentService.getTopParentId());
		
		System.out.println("Get comments by ids...");
		List<Integer> ids = new ArrayList<>(Arrays.asList(6, 9, 14)); 
		for(Comment comm: commentService.getCommentsByIds(ids)){
			System.out.println(comm.getId() + ":" + comm.getParentId());
		}
		
		//could create stored proc but database "in-memory"
		System.out.println("Get comments ids by parent id...");
		for(Integer currentID: commentService.getCommentIdsByParentId(1)){
			System.out.println("ID:" + currentID);
		}
		
		System.out.println("Get all childs test...");
		//I KNOW, THAT IN TEST DATA MANY CHILDS CONTAINS COMMENT WITH ID = 1
		Comment predeterminedComment = commentService.getCommentById(1);
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(predeterminedComment);
		while(!commentsQueue.isEmpty()) {
			Comment currentComment = commentsQueue.remove();
			List<Comment> childs = commentService.getCommentChilds(currentComment.getId());
			for(Comment child: childs) {
				commentsQueue.add(child);
				resultComments.add(child);
			}
		}
		System.out.println("Comment " + predeterminedComment.getId() + " childs:");
		for(Comment commRes: resultComments) {
			System.out.println(commRes.getId() + ":" + commRes.getParentId());
		}
	}
}
