package testlabinvent.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Objects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import testlabinvent.config.PersistenceJPAConfig;
import testlabinvent.config.RedisConfig;
import testlabinvent.data.model.Comment;
import testlabinvent.data.services.CommentService;
import testlabinvent.data_redis.services.CommentsCacheService;
import testlabinvent.data_redis.model.CommentsCache;

@RunWith(SpringRunner.class) 
@ContextConfiguration(classes = {PersistenceJPAConfig.class, RedisConfig.class}, loader = AnnotationConfigContextLoader.class)
public class GetCommentsTreePageableSketch_wrong1 {
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	CommentsCacheService commentsCacheService;
	
	private final String sessionUUID = "22122017";
	
	@Test
	public void getCommentsTreePage() {
		
		this.fillData();
		this.getInCorrectTreeOrder();
		
		//CLEAR REDIS DATABASE BEFORE START
		commentsCacheService.deleteCommentsCacheById(sessionUUID);
		System.out.println("---Fetch data page by page---");
		for(int i = 0; i < 4; i++) {
			List<Comment> fetchedComments = new ArrayList<Comment>();
			//fetchedComments = this.fetchNextComment(i, 4, sessionUUID);
			//--->FOR CHACHE FETCHING TEST
			if(i > 0) {
				
				fetchedComments = this.FETCH_CACHE_TEST(i, 4, sessionUUID);
				
			} else {
				
				fetchedComments = this.fetchNextComment(i, 4, sessionUUID);
				
			}
			//--->END FOR CACHE FETCHING TEST
			try{
				
				Thread.sleep(500);
				
			} catch(Exception e) {
				
				e.printStackTrace();
			}
			
			System.out.println("---Page-" + i + " ---");
			
			for(Comment comm:fetchedComments) {
				
				System.out.println(comm.getId() + ":" + comm.getParentId());
				
			}
		}
		
	}
	
	//MAIN METHODS
	//GET NEXT COMMENT PAGE
	private List<Comment> fetchNextComment(Integer pageNumber, Integer pageSize, String sessionUUID) {
		
		List<Comment> resultCommentsPage = new ArrayList<Comment>();
		Integer currentRootId = 0;
		
		if(commentsCacheService.getCommentsCacheById(sessionUUID) == null) {
			
			currentRootId = this.fetchNextTopId(0);
			
		} else {
			
			currentRootId = this.fetchNextTopId(commentsCacheService.getCommentsCacheById(sessionUUID).getCommentParent());
		}
		
		resultCommentsPage.add(commentService.getCommentById(currentRootId));
		resultCommentsPage.addAll(this.getAllChilds(currentRootId));
		resultCommentsPage = this.sortInTreeOrder(resultCommentsPage);
		commentsCacheService.saveCommentsCache(new CommentsCache(sessionUUID, null, currentRootId));
		//--
		if(resultCommentsPage.size() > pageSize){
			
			this.saveRestCommentsInCache(sessionUUID,resultCommentsPage, currentRootId, pageSize);
			this.getNextCachedPage(sessionUUID, currentRootId, pageSize);
			
		}	
		//--
		
		return resultCommentsPage;
	}
	
	//TEST METHOD FOR FETCHING CACHED COMMENTS
	private List<Comment> FETCH_CACHE_TEST(Integer pageNumber, Integer pageSize, String sessionUUID) {
		List<Comment> resultCommentsPage = new ArrayList<Comment>();
		
		if(commentsCacheService.getCommentsCacheById(sessionUUID).getChildsIDs().size() > 0){
			resultCommentsPage = this.getNextCachedPage(sessionUUID, 
														commentsCacheService.getCommentsCacheById(sessionUUID).getCommentParent(), 
														pageSize);
		}
		return resultCommentsPage;
	}
	
	
	//SAVE REST OF CHILD COMMENTS IN CACHE
	private List<Comment> saveRestCommentsInCache(String sessionUUID, 
													List<Comment> commentsForCaching, 
													Integer rootComment, 
													Integer pageSize) {
		
		List<Integer> cachedIds = new ArrayList<Integer>();
		for(Comment comm:commentsForCaching.subList(pageSize, commentsForCaching.size())) {
			cachedIds.add(comm.getId());
		}
		//System.out.println("Cache:" + cachedIds + "Result size:" + commentsForCaching.subList(0, pageSize).size());
		commentsCacheService.saveCommentsCache(new CommentsCache(sessionUUID,cachedIds,rootComment));
		return commentsForCaching.subList(0, pageSize);
	}
	
	//GET NEXT CACHED COMMENTS PAGE
	private List<Comment> getNextCachedPage(String sessionUUID, 
											Integer rootComment, 
											Integer pageSize) {
		
		List<Comment> nextCommentsPage = new ArrayList<Comment>();
		List<Integer> cachedCommentsIds = new ArrayList<Integer>();
		cachedCommentsIds = commentsCacheService.getCommentsCacheById(sessionUUID).getChildsIDs();
		
		nextCommentsPage = commentService.getCommentsByIds(cachedCommentsIds);
		nextCommentsPage = this.sortCommentsInIdsListOrder(nextCommentsPage, cachedCommentsIds);
		//MUST REMOVE AFTER TEST
		System.out.println("Cached ids:" + cachedCommentsIds);	
		for(Comment comm: nextCommentsPage) {
			System.out.print(comm.getId() + ":");
		}
		System.out.println("");
		//END MUST REMOVE AFTER TEST
		
		if(nextCommentsPage.size() > pageSize) {
			
			nextCommentsPage = this.saveRestCommentsInCache(sessionUUID, nextCommentsPage, rootComment, pageSize);
		}
		
		return nextCommentsPage;
	}
	
	//FETCH(GET) NEXT ROOT COMMENT ID
	private Integer fetchNextTopId(Integer previousId) {
		
		SortedSet<Integer> topComments = new TreeSet<Integer>();
		Integer topParentId  = commentService.getTopParentId();
		topComments.addAll(commentService.getCommentIdsByParentId(topParentId));
		
		if(previousId == 0) {
						
			return topComments.first();
			
		} else {
			
			Iterator setIterator = topComments.tailSet(previousId).iterator();
			setIterator.next();
			
			return (Integer)setIterator.next();
		}		
	}
	
	//END MAIN METHODS
	
	//SERVICE METHODS
	private void fillData() {
		
		System.out.println("Filling test data...");
		commentService.addComment("text", "user", 0);//1
		commentService.addComment("text", "user", 1);//2
		commentService.addComment("text", "user", 1);//3
		commentService.addComment("text", "user", 0);//4
		commentService.addComment("text", "user", 2);//5
		commentService.addComment("text", "user", 0);//6
		commentService.addComment("text", "user", 5);//7
		commentService.addComment("text", "user", 7);//8
		commentService.addComment("text", "user", 1);//9
		commentService.addComment("text", "user", 6);//10
		commentService.addComment("text", "user", 9);//11
		commentService.addComment("text", "user", 1);//12
		commentService.addComment("text", "user", 12);//13
		commentService.addComment("text", "user", 2);//14
		commentService.addComment("text", "user", 10);//15
		commentService.addComment("text", "user", 10);//16
		
	}
	
	private void getInCorrectTreeOrder() {
		List<Comment> selectedCommentsResult = new ArrayList<Comment>();
		selectedCommentsResult = commentService.getCommentsPage(0, 20);
		List<Comment> sortedComments = new ArrayList<Comment>();
		sortedComments = this.sortInTreeOrder(selectedCommentsResult);
		System.out.println("Selected comments after sort in tree order:");
		for(Comment comm: sortedComments) {
			System.out.println(comm.getId() + ":" + comm.getParentId());
		}
	}
	
	private List<Comment> sortInTreeOrder(List<Comment> selectedComments){
		//COMPARE_BY_PARENTID
		Collections.sort(selectedComments, Comment.COMPARE_BY_PARENTID);
		List<Comment> commentsBuff = new ArrayList<Comment>();
		List<Comment> commentsResult = new ArrayList<Comment>();
		commentsResult.addAll(selectedComments);
		for(Comment comm: selectedComments) {
			commentsBuff.clear();
			
			for(Comment comm2: selectedComments) {
				if(comm2.getParentId() == comm.getId()){
					commentsBuff.add(comm2);
				}
			}
			if(commentsBuff.isEmpty() == false) {
				
				commentsResult.removeAll(commentsBuff);
				Collections.reverse(commentsBuff);
				Collections.reverse(commentsResult);
				commentsResult.addAll(commentsResult.indexOf(comm), commentsBuff);	
				Collections.reverse(commentsResult);
				
			}
		}
		
		return commentsResult;
	}
	
	private List<Comment> getAllChilds(Integer parentCommentId){
		
		Comment predeterminedComment = commentService.getCommentById(parentCommentId);
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(predeterminedComment);
		while(!commentsQueue.isEmpty()) {
			Comment currentComment = commentsQueue.remove();
			List<Comment> childs = commentService.getCommentChilds(currentComment.getId());
			for(Comment child: childs) {
				commentsQueue.add(child);
				resultComments.add(child);
			}
		}
		
		return resultComments;
	}
	
	private List<Comment> sortCommentsInIdsListOrder(List<Comment> commentsList, List<Integer> indexesList) {
		
		List<Comment> sortedList = new ArrayList<Comment>();

		for(Integer index: indexesList) {
			Comment comm = commentsList.stream().filter(item -> Objects.equals(item.getId(), index)).collect(Collectors.toList()).get(0);
			sortedList.add(comm);
		}	
		
		return sortedList;
	}
}
