package testlabinvent.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import testlabinvent.config.PersistenceJPAConfig;
import testlabinvent.config.RedisConfig;
import testlabinvent.data.services.CommentService;
import testlabinvent.data_redis.services.CommentsCacheService;
import testlabinvent.data_redis.model.CommentsCache;
import testlabinvent.data.model.Comment;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


@RunWith(SpringRunner.class) 
@ContextConfiguration(classes = {PersistenceJPAConfig.class, RedisConfig.class}, loader = AnnotationConfigContextLoader.class)
public class GetCommantsTreePageableSketch_wrong0 {

	@Autowired
	CommentService commentService;
	
	@Autowired
	CommentsCacheService commentsCacheService;
	
	@Test
	public void getCommentsTreePage() {
		
		System.out.println("Filling test data...");
		commentService.addComment("text", "user", 0);//1
		commentService.addComment("text", "user", 1);//2
		commentService.addComment("text", "user", 1);//3
		commentService.addComment("text", "user", 0);//4
		commentService.addComment("text", "user", 2);//5
		commentService.addComment("text", "user", 0);//6
		commentService.addComment("text", "user", 5);//7
		commentService.addComment("text", "user", 7);//8
		commentService.addComment("text", "user", 1);//9
		commentService.addComment("text", "user", 6);//10
		commentService.addComment("text", "user", 9);//11
		commentService.addComment("text", "user", 1);//12
		commentService.addComment("text", "user", 12);//13
		commentService.addComment("text", "user", 2);//14
		commentService.addComment("text", "user", 10);//15
		commentService.addComment("text", "user", 10);//16
		List<Comment> selectedCommentsResult = new ArrayList<Comment>();
		System.out.println("Selected comments:");
		for(Comment comm: commentService.getCommentsPage(0, 20)) {
			System.out.println(comm.getId() + ":" + comm.getParentId());
			selectedCommentsResult.add(comm);
		}
		List<Comment> sortedComments = new ArrayList<Comment>();
		sortedComments = this.sortInTreeOrder(selectedCommentsResult);
		System.out.println("Selected comments after sort in tree order:");
		for(Comment comm: sortedComments) {
			System.out.println(comm.getId() + ":" + comm.getParentId());
		}
		//FETCHING COMMENTS PAGE BY PAGE
		System.out.println("Fetching comments page by page...");
		//CLEAR DATABASE BEFORE TEST
		commentsCacheService.deleteCommentsCacheById("2127");
		for(int i = 0; i < 4; i++) { // MUST TEST FOR OVERSIZE i 
			List<Comment> fetchedComments = new ArrayList<Comment>();
			fetchedComments = this.getNextCommentsPage(4, "2127");
			try{
				
				Thread.sleep(500);
				
			} catch(Exception e) {
				
				e.printStackTrace();
			}
			System.out.println("Page " + i + ":");
			for(Comment comm:fetchedComments) {
				
				System.out.println(comm.getId() + ":" + comm.getParentId());
				
			}
		}
		
		
	}
	
	private List<Comment> getNextCommentsPage(Integer pageSize, String sessionUUID) {
		
		List<Comment> commentsPage = new ArrayList<Comment>();
		CommentsCache currentCommentsCache = commentsCacheService.getCommentsCacheById(sessionUUID);
		
		if((currentCommentsCache == null) || (currentCommentsCache.getCommentParent() == null)) { //FIRST COMMENT
			
			Integer firstCommentId = commentService.getCommentChilds(commentService.getTopParentId()).get(0).getId();//+
			commentsPage = this.fetchNextComment(firstCommentId, pageSize, sessionUUID);
			
		} else if(currentCommentsCache.getChildsIDs().size() == 0) { //NOT FIRST COMMENT BUT WITHOUT CACHE
			Integer index = commentService.getCommentChilds(commentService.getTopParentId()).indexOf(currentCommentsCache.getCommentParent());
			Integer firstCommentId = commentService.getCommentChilds(commentService.getTopParentId()).get(index + 1).getId();
			//CLEAR CACHE
			commentsCacheService.deleteCommentsCacheById(sessionUUID);
			commentsPage = this.fetchNextComment(firstCommentId, pageSize, sessionUUID);
			
		} else { //NOT FIRST COMMENT WITH CACHE
			
			List<Comment> cachedComments = new ArrayList<Comment>();
			cachedComments = commentService.getCommentsByIds(currentCommentsCache.getChildsIDs());
			//
			List<Comment> cachedCommentsToDisplay = new ArrayList<Comment>();
			cachedCommentsToDisplay.addAll(cachedComments.subList(0, pageSize));							
			cachedComments.removeAll(cachedCommentsToDisplay);
			if(cachedComments.size() > 0){
				//update comments cache
				List<Integer> bufferToUpdateSet = new ArrayList<Integer>();
				for(Comment comm: cachedComments) {
					bufferToUpdateSet.add(comm.getId());
				}
				commentsCacheService.saveCommentsCache(new CommentsCache(sessionUUID,bufferToUpdateSet,1));
				//end update comments page
			} else {
				//CLEAR CACHE
				commentsCacheService.deleteCommentsCacheById(sessionUUID);
				
			}
			commentsPage.addAll(cachedCommentsToDisplay);//!!!
			commentsPage = this.sortInTreeOrder(commentsPage);
					
			//IF PAGESIZE < 4
			if(commentsPage.size() < pageSize){
				Integer index = commentService.getCommentChilds(commentService.getTopParentId()).indexOf(currentCommentsCache.getCommentParent());
				Integer firstCommentId = commentService.getCommentChilds(commentService.getTopParentId()).get(index + 1).getId();	
				List<Comment> nextComments = this.fetchNextComment(firstCommentId, pageSize - commentsPage.size(), sessionUUID);
				commentsPage.addAll(nextComments);
			}		
		}

		return commentsPage;
	}
				//save returned last number in cache							//id of next fetching comment
	private List<Comment> fetchNextComment(Integer firstCommentId, Integer pageSize, String sessionUUID) {
		
		List<Comment> resultComments = new ArrayList<Comment>();
		Integer topParentId = commentService.getTopParentId();
		List<Integer> topParents = new ArrayList<Integer>();//+
		topParents.addAll(commentService.getCommentIdsByParentId(topParentId));
		topParents.sort(null);
		for(int i = topParents.indexOf(firstCommentId); i < topParents.size(); i++) {
			Integer currentCommentId = topParents.get(i);
			resultComments = this.getAllChilds(currentCommentId);
			resultComments.add(commentService.getCommentById(currentCommentId));
			resultComments = this.sortInTreeOrder(resultComments);
			
			if(resultComments.size() > pageSize) {
				//save in cache
				List<Comment> cacheComments = resultComments.subList(pageSize, resultComments.size());
				List<Integer> cacheCommentsIds = new ArrayList<Integer>();
				for(Comment comm: cacheComments) {
					cacheCommentsIds.add(comm.getId());
				}
				CommentsCache commentsCache = new CommentsCache(sessionUUID,cacheCommentsIds,firstCommentId);
				commentsCacheService.saveCommentsCache(commentsCache);
				//remove excess values
				resultComments.removeAll(cacheComments);
				
				break;	
				
			} else {
				CommentsCache commentsCache = new CommentsCache(sessionUUID,null,firstCommentId);
				commentsCacheService.saveCommentsCache(commentsCache);
				
				continue;
			}
		}
		
		return resultComments;
	}
	
	private List<Comment> sortInTreeOrder(List<Comment> selectedComments){
		//COMPARE_BY_PARENTID
		Collections.sort(selectedComments, Comment.COMPARE_BY_PARENTID);
		List<Comment> commentsBuff = new ArrayList<Comment>();
		List<Comment> commentsResult = new ArrayList<Comment>();
		commentsResult.addAll(selectedComments);
		for(Comment comm: selectedComments) {
			commentsBuff.clear();
			
			for(Comment comm2: selectedComments) {
				if(comm2.getParentId() == comm.getId()){
					commentsBuff.add(comm2);
				}
			}
			if(commentsBuff.isEmpty() == false) {
				
				commentsResult.removeAll(commentsBuff);
				Collections.reverse(commentsBuff);
				Collections.reverse(commentsResult);
				commentsResult.addAll(commentsResult.indexOf(comm), commentsBuff);	
				Collections.reverse(commentsResult);
				
			}
		}
		
		return commentsResult;
	}
	
	private List<Comment> getAllChilds(Integer parentCommentId){
		
		Comment predeterminedComment = commentService.getCommentById(parentCommentId);
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(predeterminedComment);
		while(!commentsQueue.isEmpty()) {
			Comment currentComment = commentsQueue.remove();
			List<Comment> childs = commentService.getCommentChilds(currentComment.getId());
			for(Comment child: childs) {
				commentsQueue.add(child);
				resultComments.add(child);
			}
		}
		
		return resultComments;
	}
}
