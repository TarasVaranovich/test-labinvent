package testlabinvent.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.Map;
import java.util.HashMap;
import java.lang.Math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import testlabinvent.config.PersistenceJPAConfig;
import testlabinvent.config.RedisConfig;
import testlabinvent.data.model.Comment;
import testlabinvent.data.services.CommentService;
import testlabinvent.data_redis.model.CommentsCache;
import testlabinvent.data_redis.services.CommentsCacheService;

@RunWith(SpringRunner.class) 
@ContextConfiguration(classes = {PersistenceJPAConfig.class, RedisConfig.class}, loader = AnnotationConfigContextLoader.class)
public class GetCommentsTreePageableSketch {
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	CommentsCacheService commentsCacheService;
	
	private final String sessionUUID = "22122017";
	private final String pageKey = "25122017";
	private final Integer pageSize = 4;
	private final Integer allCommentsCount = 54;
	
	@Test
	public void getCommentsTreePage() {
		
		this.fillData();
		this.getInCorrectTreeOrder();
		
		//CLEAR REDIS DATABASE BEFORE START
		commentsCacheService.deleteCommentsCacheById(sessionUUID);
		System.out.println("---Fetch data page by page---");
		Integer pagesCount = (int)Math.floor(allCommentsCount/pageSize) + 2;
				//pagesCount
		for(int i = 0; i < 20; i++) {
			List<Comment> fetchedComments = new ArrayList<Comment>();
			fetchedComments = this.fetchNextComment(i, pageSize, sessionUUID);
			
			while(fetchedComments.size() < pageSize) {
				
				List<Comment> restOfComments = this.fetchNextComment(i, pageSize - fetchedComments.size(), sessionUUID);
				
				if(restOfComments.size() == 0) {
					
					break;
				} else {
					
					fetchedComments.addAll(restOfComments);
				}				
			}
			
			if(fetchedComments.size() == 0) {
				
				break;
			}	
			
			try{
				
				Thread.sleep(300);
				
			} catch(Exception e) {
				
				e.printStackTrace();
			}
			
			System.out.println("---Page-" + i + " ---");
			//save pages in cache
			this.savePageData(fetchedComments, pageKey + i);
			//end save pages in cache
			
			for(Comment comm:fetchedComments) {
				
				System.out.println(comm.getId() + ":" + comm.getParentId());
				
			}
		}
		System.out.println("Recieving pages from cache...");
		
		for(int i = 0; i < 20; i++) {
			List<Comment> pageContent = new ArrayList<Comment>();
			pageContent = this.getPageData(pageKey + i);
			System.out.println("Page " + i + " (data from cahche):");
			for(Comment comm: pageContent) {
				System.out.println(comm.getId() + ":" + comm.getParentId());
			}
		}
	}
	
	//MAIN METHODS
	//GET NEXT COMMENT PAGE
	private List<Comment> fetchNextComment(Integer pageNumber, Integer pageSize, String sessionUUID) {
		
		List<Comment> resultCommentsPage = new ArrayList<Comment>();
		Integer currentRootId = 0;
		List<Integer> childsInCache = new ArrayList();
		
		if(commentsCacheService.getCommentsCacheById(sessionUUID) == null) {
			
			currentRootId = this.fetchNextTopId(0);
			
			//Duplicate!!!!
			resultCommentsPage.add(commentService.getCommentById(currentRootId));
			resultCommentsPage.addAll(this.getAllChilds(currentRootId));
			resultCommentsPage = this.sortInTreeOrder(resultCommentsPage);
			
			
			if(resultCommentsPage.size() > pageSize) {
				
				childsInCache = this.saveRestInCache(resultCommentsPage, pageSize);
				resultCommentsPage = resultCommentsPage.subList(0, pageSize);
				
			}
			
		} else if(commentsCacheService.getCommentsCacheById(sessionUUID).getChildsIDs().size() > 0) {
			
			//System.out.println("Fetching cache...");
			List<Comment> fetchedComments = new ArrayList<Comment>();
			List<Integer> cachedIds = new ArrayList<Integer>();
			Map<String, List<?>> resultMap = new HashMap<String, List<?>>();
			resultMap = this.fetchCommentsFromCache(sessionUUID, pageSize);
			List<?> fetchedCommentsGen = this.fetchCommentsFromCache(sessionUUID, pageSize).get("fetchedComments");
			List<?> cachedIdsGen = this.fetchCommentsFromCache(sessionUUID, pageSize).get("cachedIds");
			fetchedComments = (List<Comment>)(Object)fetchedCommentsGen;
			cachedIds = (List<Integer>)(Object)cachedIdsGen;
			childsInCache = cachedIds;
			resultCommentsPage = fetchedComments;
			currentRootId = commentsCacheService.getCommentsCacheById(sessionUUID).getCommentParent();
			
		} else {
					
			currentRootId = this.fetchNextTopId(commentsCacheService.getCommentsCacheById(sessionUUID).getCommentParent());
			 //Duplicate!!!!
			if(currentRootId != null) {
				
				resultCommentsPage.add(commentService.getCommentById(currentRootId));
				resultCommentsPage.addAll(this.getAllChilds(currentRootId));
				resultCommentsPage = this.sortInTreeOrder(resultCommentsPage);
				
			} else {
				
				resultCommentsPage.clear();
				
			}	
			
			if(resultCommentsPage.size() > pageSize) {
				
				childsInCache = this.saveRestInCache(resultCommentsPage, pageSize);
				resultCommentsPage = resultCommentsPage.subList(0, pageSize);
				
			}
			
		}
		
		if(currentRootId != null) {
			
			commentsCacheService.saveCommentsCache(new CommentsCache(sessionUUID, childsInCache, currentRootId));
			
		}
			
		return resultCommentsPage;
	}
	
	private List<Integer> saveRestInCache(List<Comment> oversizePage, Integer pageSize) {
		
		List<Integer> cachedIds = new ArrayList<Integer>();
		oversizePage = oversizePage.subList(pageSize, oversizePage.size());
		
		for(Comment comm: oversizePage) {
			
			cachedIds.add(comm.getId());
			
		}
		
		return cachedIds;
	}
	
	private Map<String, List<?>> fetchCommentsFromCache(String sessionUUID, Integer pageSize) {
		
		List<Comment> fetchedComments = new ArrayList<Comment>();
		List<Integer> fetchedIds = new ArrayList<Integer>();
		fetchedIds = commentsCacheService.getCommentsCacheById(sessionUUID).getChildsIDs();
		fetchedComments = commentService.getCommentsByIds(fetchedIds);
		fetchedComments = this.sortCommentsInIdsListOrder(fetchedComments, fetchedIds);
		fetchedIds.clear();
		
		if(fetchedComments.size() > pageSize) {
			
			fetchedIds = this.saveRestInCache(fetchedComments, pageSize);
			fetchedComments = fetchedComments.subList(0, pageSize);
			
		} 
		HashMap<String,  List<?>> result = new HashMap<String, List<?>>();
		result.put("fetchedComments", fetchedComments);
		result.put("cachedIds", fetchedIds);
		
		return result;
	}
	
	private Integer fetchNextTopId(Integer previousId) {
		
		SortedSet<Integer> topComments = new TreeSet<Integer>();
		Integer topParentId  = commentService.getTopParentId();
		topComments.addAll(commentService.getCommentIdsByParentId(topParentId));
		
		if(previousId == 0) {
						
			return topComments.first();
			
		} else {
			
			Iterator setIterator = topComments.tailSet(previousId).iterator();
			setIterator.next();
			
			if(topComments.tailSet(previousId).size() > 1) {
				
				return (Integer)setIterator.next();
				
			} else {
				
				return null;
			}		
		}		
	}
	//END MAIN METHODS
	//CACHING PAGES
	private void savePageData(List<Comment> pageComments, String pageId) {
		List<Integer> commentsIds = new ArrayList<Integer>();
		for(Comment comm: pageComments) {
			
			commentsIds.add(comm.getId());
			
		}
		CommentsCache commentsCache = new CommentsCache(pageId,commentsIds, null);
		commentsCacheService.saveCommentsCache(commentsCache);
	}
	
	private List<Comment> getPageData(String pageId) {
		List<Comment> pageData = new ArrayList<Comment>();
		List<Integer> cachedIds = new ArrayList<Integer>();
		cachedIds = commentsCacheService.getCommentsCacheById(pageId).getChildsIDs();
		pageData = commentService.getCommentsByIds(cachedIds);
		pageData = this.sortCommentsInIdsListOrder(pageData, cachedIds);
		
		return pageData;
	}
	//END CACHING PAGES
	
	//SERVICE METHODS
	private void fillData() {
		
		System.out.println("Filling test data...");
		commentService.addComment("text", "user", 0);//1
		commentService.addComment("text", "user", 1);//2
		commentService.addComment("text", "user", 1);//3
		commentService.addComment("text", "user", 0);//4
		commentService.addComment("text", "user", 2);//5
		commentService.addComment("text", "user", 0);//6
		commentService.addComment("text", "user", 5);//7
		commentService.addComment("text", "user", 7);//8
		commentService.addComment("text", "user", 1);//9
		commentService.addComment("text", "user", 6);//10
		commentService.addComment("text", "user", 9);//11
		commentService.addComment("text", "user", 1);//12
		commentService.addComment("text", "user", 12);//13
		commentService.addComment("text", "user", 2);//14
		commentService.addComment("text", "user", 10);//15
		commentService.addComment("text", "user", 10);//16
		//--
		commentService.addComment("text", "user", 0);//17
		commentService.addComment("text", "user", 17);//18
		commentService.addComment("text", "user", 18);//19
		commentService.addComment("text", "user", 17);//20
		commentService.addComment("text", "user", 18);//21
		commentService.addComment("text", "user", 19);//22
		commentService.addComment("text", "user", 19);//23
		commentService.addComment("text", "user", 20);//24
		commentService.addComment("text", "user", 21);//25
		commentService.addComment("text", "user", 20);//26
		commentService.addComment("text", "user", 17);//27
		commentService.addComment("text", "user", 20);//28
		commentService.addComment("text", "user", 28);//29
		commentService.addComment("text", "user", 27);//30
		commentService.addComment("text", "user", 26);//31
		commentService.addComment("text", "user", 27);//32
		commentService.addComment("text", "user", 0);//33
		commentService.addComment("text", "user", 0);//34
		commentService.addComment("text", "user", 0);//35
		commentService.addComment("text", "user", 35);//36
		commentService.addComment("text", "user", 35);//37
		commentService.addComment("text", "user", 35);//38
		commentService.addComment("text", "user", 36);//39
		commentService.addComment("text", "user", 35);//40
		commentService.addComment("text", "user", 37);//41
		commentService.addComment("text", "user", 36);//42
		commentService.addComment("text", "user", 40);//43
		commentService.addComment("text", "user", 35);//44
		commentService.addComment("text", "user", 37);//45
		commentService.addComment("text", "user", 36);//46
		commentService.addComment("text", "user", 37);//47
		commentService.addComment("text", "user", 43);//48
		commentService.addComment("text", "user", 48);//49
		commentService.addComment("text", "user", 49);//50	
		commentService.addComment("text", "user", 0);//51	
		commentService.addComment("text", "user", 0);//52	
		commentService.addComment("text", "user", 0);//53	
		commentService.addComment("text", "user", 0);//54
		
	}
	
	private void getInCorrectTreeOrder() {
		List<Comment> selectedCommentsResult = new ArrayList<Comment>();
		selectedCommentsResult = commentService.getCommentsPage(0, allCommentsCount);
		List<Comment> sortedComments = new ArrayList<Comment>();
		sortedComments = this.sortInTreeOrder(selectedCommentsResult);
		System.out.println("Selected comments after sort in tree order:");
		for(Comment comm: sortedComments) {
			System.out.println(comm.getId() + ":" + comm.getParentId());
		}
	}
	
	private List<Comment> sortInTreeOrder(List<Comment> selectedComments){
		//COMPARE_BY_PARENTID
		Collections.sort(selectedComments, Comment.COMPARE_BY_PARENTID);
		List<Comment> commentsBuff = new ArrayList<Comment>();
		List<Comment> commentsResult = new ArrayList<Comment>();
		commentsResult.addAll(selectedComments);
		for(Comment comm: selectedComments) {
			commentsBuff.clear();
			
			for(Comment comm2: selectedComments) {
				if(comm2.getParentId() == comm.getId()){
					commentsBuff.add(comm2);
				}
			}
			if(commentsBuff.isEmpty() == false) {
				
				commentsResult.removeAll(commentsBuff);
				Collections.reverse(commentsBuff);
				Collections.reverse(commentsResult);
				commentsResult.addAll(commentsResult.indexOf(comm), commentsBuff);	
				Collections.reverse(commentsResult);
				
			}
		}
		
		return commentsResult;
	}
	
	private List<Comment> getAllChilds(Integer parentCommentId){
		
		Comment predeterminedComment = commentService.getCommentById(parentCommentId);
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(predeterminedComment);
		while(!commentsQueue.isEmpty()) {
			Comment currentComment = commentsQueue.remove();
			List<Comment> childs = commentService.getCommentChilds(currentComment.getId());
			for(Comment child: childs) {
				commentsQueue.add(child);
				resultComments.add(child);
			}
		}
		
		return resultComments;
	}
	
	private List<Comment> sortCommentsInIdsListOrder(List<Comment> commentsList, List<Integer> indexesList) {
		
		List<Comment> sortedList = new ArrayList<Comment>();

		for(Integer index: indexesList) {
			Comment comm = commentsList.stream().filter(item -> Objects.equals(item.getId(), index)).collect(Collectors.toList()).get(0);
			sortedList.add(comm);
		}	
		
		return sortedList;
	}
}
