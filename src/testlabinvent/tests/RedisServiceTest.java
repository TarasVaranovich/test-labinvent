package testlabinvent.tests;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import testlabinvent.config.RedisConfig;
import testlabinvent.data_redis.jpa.CommentsCacheRepository;
import testlabinvent.data_redis.model.CommentsCache;
import testlabinvent.data_redis.services.CommentsCacheService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = RedisConfig.class, loader = AnnotationConfigContextLoader.class)
public class RedisServiceTest {
	
	@Autowired(required = true)
	CommentsCacheService commentsCacheService;
	
	@Test
	public void redisTest(){
		
		List<Integer> testIDs = new ArrayList<Integer>();
		testIDs.add(67);
		testIDs.add(89);
		testIDs.add(103);
		
		CommentsCache commentsCacheTest = new CommentsCache("24",testIDs, 45);
		System.out.println("Save new comments cache...");
		commentsCacheService.saveCommentsCache(commentsCacheTest);
		String summaryID = commentsCacheTest.getUUIDString();
		System.out.println("Getting exists comments cache...");
		System.out.println("Comment " + summaryID + " childs:" + commentsCacheService.getCommentsCacheById(summaryID).getChildsIDs());
		System.out.println("Deleting comments cache...");
		commentsCacheService.deleteCommentsCacheById(summaryID);
		System.out.println("Trying to get removed comments cache...");
		System.out.println("Comment " + summaryID + " childs:" + commentsCacheService.getCommentsCacheById(summaryID).toString());
		System.out.println("Trying to delete comments cache one more time...");
		commentsCacheService.deleteCommentsCacheById(summaryID);
	}
}
