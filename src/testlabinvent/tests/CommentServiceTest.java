package testlabinvent.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import testlabinvent.config.PersistenceJPAConfig;
import testlabinvent.data.services.CommentService;
import testlabinvent.data.model.Comment;

import org.springframework.beans.factory.annotation.Autowired;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenceJPAConfig.class, loader = AnnotationConfigContextLoader.class)
public class CommentServiceTest {
	
	@Autowired
	CommentService commentService;
	
	@Test
	public void commentServiceTest() {
		System.out.println("Begin comment service test...");
		System.out.println("Adding comments...");
		for(int i = 0; i < 10; i++) {
			if(i < 3){
				commentService.addComment("text" + i, "user1", 0);
			} else if(i < 6) {
				commentService.addComment("text" + i, "user2", 1);
			} else {
				commentService.addComment("text" + i, "user3", 2);
			}
		}
		System.out.println("Getting comments page from database...");
		for(Comment comm: commentService.getCommentsPage(0, 10)) {
			System.out.println(comm.getId() + ":" + 
								comm.getMessageText() + ":" + 
								comm.getUserName() + ":" + 
								comm.getParentId());
		}
		System.out.println("Getting all user coments from database...");
		for(int i = 1; i < 4; i++) {
			System.out.println("User " + i + " comments...");
			for(Comment comm: commentService.getCommentsByUserName("user" + i, 0, 5)) {
				System.out.println(comm.getId() + ":" + 
									comm.getMessageText() + ":" + 
									comm.getUserName() + ":" + 
									comm.getParentId());
			}
		}
		System.out.println("Getting all comment childs...");
		for(int i = 0; i < 3; i++) {
			System.out.println(i + " childs...");
			for(Comment comm: commentService.getCommentChilds(i)) {
				System.out.println(comm.getId() + ":" + 
									comm.getMessageText() + ":" + 
									comm.getUserName() + ":" + 
									comm.getParentId());
			}
		}
		
	}

}
