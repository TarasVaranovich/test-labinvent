package testlabinvent.tests;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.lang.Math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import testlabinvent.config.PersistenceJPAConfig;
import testlabinvent.config.RedisConfig;
import testlabinvent.data.services.CommentService;
import testlabinvent.data_redis.model.CommentsCache;
import testlabinvent.data_redis.services.CommentsCacheService;
import testlabinvent.data.model.Comment;

@RunWith(SpringRunner.class) 
@ContextConfiguration(classes = {PersistenceJPAConfig.class, RedisConfig.class}, loader = AnnotationConfigContextLoader.class)
public class SearchInDatabaseSketch {
	
	private final String pageKey = "25122017";
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	CommentsCacheService commentsCacheService;
	
	@Test
	public void searchInDataBase() {
		System.out.println("Filling data...");
		this.fillData();
		try{
			
			Thread.sleep(300);
			
		} catch(Exception e) {
			
			e.printStackTrace();
		}
		
		List<Comment> checkFilling = new ArrayList<Comment>();
		checkFilling = commentService.getCommentsPage(0, 100);
		System.out.println("In database exist " + checkFilling.size() + " comments...");
		
		for(Comment comm: this.searchAllUserComments("David", 0, 20)) {
			System.out.println(comm.getUserName() + " wrote(" + comm.getId() + "):" + comm.getMessageText());
		}
		for(Comment comm: this.searchAllUserComments("Silvia", 0, 20)) {
			System.out.println(comm.getUserName() + " wrote(" + comm.getId() + "):" + comm.getMessageText());
		}
		String textStr = "Megatron-3000";
		for(Comment comm: this.searchTextInComments(textStr)) {
			System.out.println(textStr + " exist in comment with id=" + comm.getId());
		}
		
		System.out.println("Search comment parents...");
		Comment presettedComm = commentService.getCommentById(47); // !!!!
		Integer topParentId = this.getTopParentId(presettedComm);
		
		System.out.println("Top parent Id of comment with id " + 
							presettedComm.getId() + " is " + 
							topParentId);
		
		System.out.println("Save comments brahch in cache...");
		String keyUUID = this.pageKey + presettedComm.getId();
		commentsCacheService.deleteCommentsCacheById(keyUUID);
		List<Comment> branchComments = this.getResultBranch(topParentId);
		System.out.println("Result branch:");
		for(Comment comm: branchComments) {
			System.out.println(comm.getId() + ":" + comm.getParentId() + " index:" + branchComments.indexOf(comm));
		}
		this.saveResultBranch(branchComments, presettedComm.getId(), keyUUID);
		System.out.println("Getting comments page from cached branch...");
		List<Comment> commentsPage = new ArrayList<Comment>();
		commentsPage = this.getBranchPage(2, 3, keyUUID);	
		System.out.println("Page direct 2/3:");
		for(Comment comm: commentsPage) {
			System.out.println(comm.getId() + ":" + comm.getParentId());
		}
		System.out.println("Page backward -2/3:");
		commentsPage.clear();
		commentsPage = this.getBranchPage(-2, 3, keyUUID);	
		for(Comment comm: commentsPage) {
			System.out.println(comm.getId() + ":" + comm.getParentId());
		}
	}
	
	private List<Comment> searchAllUserComments(String userName, Integer pageNumber, Integer pageSize) {
		
		return commentService.getCommentsByUserName(userName, pageNumber, pageSize);
	}
	
	private List<Comment> searchTextInComments(String requiredTextString) {
		
		return commentService.getCommentsContainingText(requiredTextString, 0 ,100);
	}
	//METHODS FOR NAVIGATION
	private Integer getTopParentId(Comment comment){
	
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(comment);
		
		while(!commentsQueue.isEmpty()) {
			
			Comment currentComment = commentsQueue.remove();
			Comment parentComment = commentService.getCommentById(currentComment.getParentId());
			if(parentComment != null){
				commentsQueue.add(parentComment);
				resultComments.add(parentComment);
			}
			
		}

		return resultComments.get(resultComments.size() - 1).getId();
	}
	
	private List<Comment> getResultBranch(Integer topParentId){
		
		List<Comment> topParentChilds = new ArrayList<Comment>();
		topParentChilds = this.getAllChilds(topParentId);
		topParentChilds = this.sortInTreeOrder(topParentChilds);
		
		return topParentChilds;
	}
	
	private void saveResultBranch(List<Comment> commentsForSaveSorted, Integer searchedCommentId, String searchedCommentID){
		List<Integer> commentsIds = new ArrayList<Integer>();
		for(Comment comm: commentsForSaveSorted) {
			commentsIds.add(comm.getId());
		}
		commentsCacheService.saveCommentsCache(new CommentsCache(searchedCommentID, commentsIds,  searchedCommentId));
	}
	//FETCH ALL BRANCH WHOLLY, BECAUSE COMMENTS BRANCH WILL RESTRICTED (IT'S NOT BIG IN COMPARISON WITH ALL COMMENTS COUNT)
	private List<Comment> getBranchPage(Integer pageNumber, Integer pageSize, String searchedCommentID){
		
		List<Comment> commentsPage = new ArrayList<Comment>();
		List<Integer> commentIds = new ArrayList<Integer>();
		commentIds = commentsCacheService.getCommentsCacheById(searchedCommentID).getChildsIDs();
		List<Comment> allBranchComments = new ArrayList<Comment>();
		allBranchComments = commentService.getCommentsByIds(commentIds);
		allBranchComments = this.sortCommentsInIdsListOrder(allBranchComments, commentIds);
		Integer searchedCommentId = commentsCacheService.getCommentsCacheById(searchedCommentID).getCommentParent();
		Comment searchedComment = commentService.getCommentById(searchedCommentId);
		Comment commInList = allBranchComments.stream().filter(item -> Objects.equals(item.getId(), searchedCommentId)).collect(Collectors.toList()).get(0);
		Integer commentPosition = allBranchComments.indexOf(commInList);
		
		if(pageNumber >= 0) {
			
			commentsPage = allBranchComments.subList(commentPosition + pageSize*(pageNumber - 1),
														commentPosition + pageSize*pageNumber);
			
		} else {
			
			pageNumber = Math.abs(pageNumber);
			commentsPage = allBranchComments.subList(commentPosition - pageSize*pageNumber , 
														commentPosition - pageSize*(pageNumber - 1));
			
			
			
		} 
		
		return commentsPage;
	}
	//END METHODS FOR NAVIGATION
	
	//SERVICE METHODS
	private void fillData() {
		//"John" "David" "Thomas" "Donald" "Paul" , "Mary" "Nancy" "Susan" "Ruth" "Jennifer"
		System.out.println("Filling test data...");
		commentService.addComment("text bla-bla", "John", 0);//1
		commentService.addComment("text", "Thomas", 1);//2
		commentService.addComment("text", "Mary", 1);//3
		commentService.addComment("text", "David", 0);//4
		commentService.addComment("text", "John", 2);//5
		commentService.addComment("Megatron-3000", "George", 0);//6
		commentService.addComment("text", "Thomas", 5);//7
		commentService.addComment("He is anonimous", "David", 7);//8
		commentService.addComment("text", "Betty", 1);//9
		commentService.addComment("text", "Thomas", 6);//10
		commentService.addComment("next_process...", "Susan", 9);//11
		commentService.addComment("text", "Nancy", 1);//12
		commentService.addComment("text", "Ruth", 12);//13
		commentService.addComment("text", "John", 2);//14
		commentService.addComment("text", "Donald", 10);//15
		commentService.addComment("text", "David", 10);//16
		//--
		commentService.addComment("text", "Mary", 0);//17
		commentService.addComment("text", "Mary", 17);//18
		commentService.addComment("text", "Nancy", 18);//19
		commentService.addComment("text", "Susan", 17);//20
		commentService.addComment("Megatron-3000", "Donald", 18);//21
		commentService.addComment("text", "Susan", 19);//22
		commentService.addComment("text", "Ruth", 19);//23
		commentService.addComment("text", "Donald", 20);//24
		commentService.addComment("Megatron-3000", "Thomas", 21);//25
		commentService.addComment("text", "Mary", 20);//26
		commentService.addComment("text", "Jennifer", 17);//27
		commentService.addComment("text", "Ruth", 20);//28
		commentService.addComment("text", "Paul", 28);//29
		commentService.addComment("$Min%/max", "Mary", 27);//30
		commentService.addComment("text", "Jennifer", 26);//31
		commentService.addComment("text", "David", 27);//32
		commentService.addComment("text", "Paul", 0);//33
		commentService.addComment("text", "Susan", 0);//34
		commentService.addComment("text", "Ruth", 0);//35
		commentService.addComment("text", "Mark", 35);//36
		commentService.addComment("text", "Donald", 35);//37
		commentService.addComment("text", "Susan", 35);//38
		commentService.addComment("12345 ....???", "Marry", 36);//39
		commentService.addComment("text", "Jennifer", 35);//40
		commentService.addComment("text", "Mark", 37);//41
		commentService.addComment("text", "John", 36);//42
		commentService.addComment("text", "Ruth", 40);//43
		commentService.addComment("text", "Nancy", 35);//44
		commentService.addComment("text", "Marry", 37);//45
		commentService.addComment("This song is pretty good, but musician is bad guy...", "Donald", 36);//46
		commentService.addComment("text", "Jennifer", 37);//47
		commentService.addComment("text", "Jennifer", 43);//48
		commentService.addComment("\tNot \tfor \tme ", "Ruth", 48);//49
		commentService.addComment("\nBut \nfor \nhes frends", "Jennifer", 49);//50	
		commentService.addComment("text","Jennifer", 0);//51	
		commentService.addComment("text", "Nancy", 0);//52	
		commentService.addComment("text", "Mark", 0);//53	
		commentService.addComment("text", "Betty", 0);//54
			
	}
	//END SERVICE METHODS
	//ADOPTED METHODS FROM OTHER CLASSES
	private List<Comment> getAllChilds(Integer parentCommentId){
		
		Comment predeterminedComment = commentService.getCommentById(parentCommentId);
		LinkedList<Comment> commentsQueue = new LinkedList<Comment>();
		List<Comment> resultComments = new ArrayList<Comment>();
		commentsQueue.add(predeterminedComment);
		while(!commentsQueue.isEmpty()) {
			Comment currentComment = commentsQueue.remove();
			List<Comment> childs = commentService.getCommentChilds(currentComment.getId());
			for(Comment child: childs) {
				commentsQueue.add(child);
				resultComments.add(child);
			}
		}
		
		return resultComments;
	}
	
	private List<Comment> sortInTreeOrder(List<Comment> selectedComments){
		//COMPARE_BY_PARENTID
		Collections.sort(selectedComments, Comment.COMPARE_BY_PARENTID);
		List<Comment> commentsBuff = new ArrayList<Comment>();
		List<Comment> commentsResult = new ArrayList<Comment>();
		commentsResult.addAll(selectedComments);
		for(Comment comm: selectedComments) {
			commentsBuff.clear();
			
			for(Comment comm2: selectedComments) {
				if(comm2.getParentId() == comm.getId()){
					commentsBuff.add(comm2);
				}
			}
			if(commentsBuff.isEmpty() == false) {
				
				commentsResult.removeAll(commentsBuff);
				Collections.reverse(commentsBuff);
				Collections.reverse(commentsResult);
				commentsResult.addAll(commentsResult.indexOf(comm), commentsBuff);	
				Collections.reverse(commentsResult);
				
			}
		}
		
		return commentsResult;
	}
	
	private List<Comment> sortCommentsInIdsListOrder(List<Comment> commentsList, List<Integer> indexesList) {
		
		List<Comment> sortedList = new ArrayList<Comment>();

		for(Integer index: indexesList) {
			Comment comm = commentsList.stream().filter(item -> Objects.equals(item.getId(), index)).collect(Collectors.toList()).get(0);
			sortedList.add(comm);
		}	
		
		return sortedList;
	}
	//END ADOPTED METHODS FROM OTHER CLASSES
	
}
